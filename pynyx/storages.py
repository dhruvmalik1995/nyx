from dataclasses import dataclass

from pytest_tezos.TestTypes import AbstractRecord, Address, Bool, Map, Nat, Timestamp, String, List, Bytes, Set


@dataclass
class EventSinkStorage(AbstractRecord):
    owner: Address
    permissions: Set
    messages: Map
    message_counter: Nat

    # why is this needed ? wtf ?
    def __repr__(self):
        return super().__repr__()


@dataclass
class InvestorInfo(AbstractRecord):
    country: Bytes
    region: Nat
    rating: Nat
    expires: Timestamp
    restricted: Bool

    # why is this needed ? wtf ?
    def __repr__(self):
        return super().__repr__()


@dataclass
class KYCStorage(AbstractRecord):
    owner: Address
    members: Map

    # why is this needed ? wtf ?
    def __repr__(self):
        return super().__repr__()


@dataclass
class RegistrarInfo(AbstractRecord):
    restricted: Bool

    # why is this needed ? wtf ?
    def __repr__(self):
        return super().__repr__()


@dataclass
class TokenInfo(AbstractRecord):
    restricted: Bool

    # why is this needed ? wtf ?
    def __repr__(self):
        return super().__repr__()


@dataclass
class AccountInfo(AbstractRecord):
    registrar: Address
    restricted: Bool

    # why is this needed ? wtf ?
    def __repr__(self):
        return super().__repr__()


@dataclass
class CountryRestriction(AbstractRecord):
    country_invest_limit: Nat
    vesting: Timestamp
    min_rating: Nat
    rating_restrictions: Map

    # why is this needed ? wtf ?
    def __repr__(self):
        return super().__repr__()


@dataclass
class IssuingEntityStorage(AbstractRecord):
    name: String
    owner: Address
    kyc_registrars: Map
    security_tokens: Map
    global_invest_limit: Nat
    country_restrictions: Map
    investor_counter: Nat
    country_counters: Map
    accounts: Map
    document_hashes: Map

    # why is this needed ? wtf ?
    def __repr__(self):
        return super().__repr__()


@dataclass
class SecurityTokensStorage(AbstractRecord):
    name: String
    allow_transfer_from: Map
    issuer: Address
    tokens: Nat
    agreement_procedure: Bool
    balances: Map
    symbol: String
    decimals: Nat
    owner: Address
    standalone: Bool

    # why is this needed ? wtf ?
    def __repr__(self):
        return super().__repr__()


@dataclass
class CrowdsaleStorage(AbstractRecord):
    owner: Address
    receiver: Address
    security_token: Address
    crowdsale_start: Timestamp
    crowdsale_finish: Timestamp
    crowdsale_completed: Timestamp
    bonus_psc: List
    bonus_times: List
    currencies: Map
    #tz_fiat_rate: Nat
    #token_fiat_rate: Nat
    #tokens_max: Nat
    #tokens: Nat
    fiat_max: Nat
    fiat: Nat

    # why is this needed ? wtf ?
    def __repr__(self):
        return super().__repr__()
