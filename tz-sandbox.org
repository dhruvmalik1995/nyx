#+Title: Developping on Tezos with custom gas restrictions
#+Author: Thomas Binétruy
#+INCLUDE: "print/doctype-memo.org"
{{{subtitle(Equisafe)}}}
{{{date(July 2018)}}}
{{{theme-color(19cba9)}}}

* Developping on Tezos with custom gas restrictions
  
Then next Tezos Protocol update, Carthage net, will increase the gas restrictions allowing the development of hungrier smart contracts. In this post, we document how to update these hard limits to arbitrary values letting developers implement contracts in view of protocol updates. For instance, the gas restrictions will increase by multiple folds before the end of the year.

We develop on the Tezos sandbox, the simplest is to pull Yourlabs' docker image:

#+begin_src sh
docker run -t -p 8732:8732 yourlabs/tezos
#+end_src

But since we want to modify some of the sandbox parameters, we will need to the sources to rebuild the image:

#+begin_src sh :session tuto
git clone git@yourlabs.io:oss/containers.git
cd containers/tezos
#+end_src
#+begin_src sh :session tuto
cd /home/thomas/code/containers/tezos
#+end_src

#+RESULTS:

We not want to modify the entry ~hard_gas_limit_per_operation~ and ~hard_gas_limit_per_block~ of the file ~sandbox-parametrs.json~ to 2,000,000 and 20,000,000 respectively and rebuild the image:

#+begin_src sh :session tuto
sed -i 's/"hard_gas_limit_per_operation": "[0-9]*"/"hard_gas_limit_per_operation": "10000000"/g' sandbox-parameters.json
sed -i 's/"hard_gas_limit_per_block": "[0-9]*"/"hard_gas_limit_per_block": "100000000"/g' sandbox-parameters.json
cat sandbox-parameters.json | jq .hard_gas_limit_per_operation  # => 9000000
#+end_src

#+RESULTS:
|        |
| 800000 |

We can now build and run the docker image with port redirection so we can communicate with the sandbox from outside the docker:

#+begin_src sh :session tuto
docker build -t tz-sandbox .
docker run -t -d -p 8732:8732 tz-sandbox
#+end_src

This will start the sandbox in a container bridging the standard port. You can verify that it works by running the following command:

#+begin_src sh :session tuto
curl http://localhost:8732/chains/main/blocks/head/context/constants | jq .hard_gas_limit_per_operation # => 9000000
#+end_src

As you can see, by curling the sandbox API, our constant was updated

#+begin_src sh :session tuto
curl http://localhost:8732/chains/main/blocks/head/context/constants | jq .hard_gas_limit_per_block # => 9000000
#+end_src

We will now run a michelson contract that consumes over 800,000 gas for ~--arg 260000~:

#+begin_src 
parameter int ;
storage unit ;
code { CAR ;
       DUP ; NEQ ; LOOP { PUSH int 1 ; SWAP ; SUB ; DUP ; NEQ } ;
       DROP ;
       UNIT ;
       NIL operation ;
       PAIR }
#+end_src

[1] https://tezos.gitlab.io/protocols/006_carthage.html#smart-contracts

We want to save this script to a file and originate it on the sandbox:

#+begin_src sh :session tuto
docker exec $(docker ps -q) /bin/bash -c 'echo "parameter int ; storage unit ; code { CAR ; DUP ; NEQ ; LOOP { PUSH int 1 ; SWAP ; SUB ; DUP ; NEQ } ; DROP ; UNIT ; NIL operation ; PAIR }" > test-contract.tz'
docker exec $(docker ps -q) /bin/bash -c 'head test-contract.tz' 
#+end_src

#+begin_src sh :session tuto
docker exec $(docker ps -q) /bin/bash -c 'tezos-client originate contract TEST_GAS_CONTRACT transferring 42 from bootstrap1 running test-contract.tz --burn-cap 5'
#+end_src

#+begin_src sh :session tuto
docker exec $(docker ps -q) /bin/bash -c 'tezos-client transfer 5 from bootstrap1 to TEST_GAS_CONTRACT --arg 360000'
#+end_src

As you can see, this operation used 1367765 gas which is well above the Babylon net gas limits. We can now try to execute a much hungrier contract and see that it fails as expected:

#+begin_src sh :session tuto
docker exec $(docker ps -q) /bin/bash -c 'tezos-client transfer 5 from bootstrap1 to TEST_GAS_CONTRACT --arg 3600000'
#+end_src

The operation failed as it is over the gas limit per operation.
