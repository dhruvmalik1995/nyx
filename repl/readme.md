# Introduction

This simple Python script provides a REPL-like wrapper over the `ligo` command to compile and interpret Ligo.

# Usage

The script is used as follows:

- input: Ligo expression
- output:
  + `interpret` mode: interprets the ligo expression and displays its output
  + `compile-expression` mode: compiles the ligo expression and displays its michelson representation

```sh
$ python repl.py

Input:
[1,2]
Output:
[ ( 1 , 2 ) ]

Input:
compile-expression
Output:
Message: Switched to compile-expression mode!

Input:
[1,2]
Output:
{ Pair 1 2 }

Input:
interpret
Output:
Message: Switched to interpret mode!

Input:
let increment : (int -> int) = fun (i: int) -> i + 1 in increment 1
Output:
2

Input:
let add: ((int * int) -> int) = fun (a: int * int) -> a.0 + a.1 in (add (1 , 2))
Output:
3

Input:
compile-expression
Output:
Message: Switched to compile-expression mode!

Input:
fun (a: int) (b: int) -> a + b
Output:
{ DUP ;  CAR ;  DIP { DUP } ;  SWAP ;  CDR ;  DIP { DUP } ;  SWAP ;  DIP { DUP } ;  ADD ;  DIP { DROP 3 } }
```
