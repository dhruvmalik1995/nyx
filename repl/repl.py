from subprocess import Popen, PIPE
import sys
from typing import List

MODES = {
    "compile-expression": 1,
    "interpret": 2,
}

class REPL:
    def __init__(self):
        self.mode = MODES["interpret"]
        self.loop(lambda : self.print(self.eval(self.read())))

    def call_cmd(self, *args) -> str:
        p = Popen(args, stdout=PIPE, stderr=PIPE)
        return p.communicate()[0].decode('ascii').replace('\r', '').replace('\n', '')

    def compile_expr(self, expr: str) -> str:
        return self.call_cmd(
            "ligo",
            "compile-expression",
            "cameligo",
            expr,
        )

    def interpret(self, expr: str) -> str:
        return self.call_cmd(
            "ligo",
            "interpret",
            "-s",
            "cameligo",
            expr,
        )

    def read(self, ):
        print("\nInput:")
        return sys.stdin.readline().replace("\n", "")

    def eval(self, expr):
        print("Output:")
        if expr in MODES.keys():
            self.mode = MODES[expr]
            return f"Message: Switched to {expr} mode!"
        else:
            if self.mode == MODES["interpret"]:
                return self.interpret(expr)
            elif self.mode == MODES["compile-expression"]:
                return self.compile_expr(expr)
            else:
                return NotImplementedError

    def print(self, res):
        print(res)

    def loop(self, f):
        while(1):
            f()

REPL()
