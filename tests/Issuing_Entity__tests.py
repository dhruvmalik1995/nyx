from dataclasses import dataclass
from pytest_tezos.TestTypes import Address, Bool, Map, Nat, Timestamp, AbstractTestType

from pynyx.helpers import empty_map, get_id_for_region_code
from pynyx.contracts import KYC, Issuer as IssuerContract
from pynyx.environments import IssuerEnv, EnvParams
from pynyx.storages import AccountInfo, TokenInfo, CountryRestriction

from helpers import _test_error_msg, _test_is_owner


@dataclass
class Hex(AbstractTestType):
    value: int

    def get_type(self):
        return "bytes"

    def __repr__(self):
        return "0x" + str(self.value)

    def __hash__(self):
        return hash(self.value)


country1 = get_id_for_region_code("FR-J")
country2 = get_id_for_region_code("FR-OCC")
country3 = get_id_for_region_code("FR-PAC")
class Issuer(IssuerContract):
    def get_init_accounts(self, registrar: Address) -> Map:
        """returns `storage.accounts` initial storage. We provide two accounts, one registered and one
        unregistered and set them as attributes so that they can be accessed from test methods."""
        return Map(
            {
                Address(self.tezos.addresses[0]): AccountInfo(registrar, Bool(False)),
                Address(self.tezos.addresses[1]): AccountInfo(registrar, Bool(False)),
                Address(self.tezos.addresses[2]): AccountInfo(registrar, Bool(False)),
                Address(self.tezos.addresses[3]): AccountInfo(registrar, Bool(False)),
                Address(self.tezos.addresses[4]): AccountInfo(registrar, Bool(True)),
            }
        )

    @property
    def default_registrar(self):
        return self.tezos.addresses[1]

    def get_init_security_tokens(self) -> Map:
        """return `storage.security_tokens` initial storage. We provide two tokens
        (set to random addresses), one set to true and one set to false. We pass
        their references as attributes to allow for access from test methods."""
        token_restricted = Address(self.tezos.addresses[1])
        token_unrestricted = Address(self.tezos.addresses[2])
        restricted_token_info = TokenInfo(Bool(True))
        unrestricted_token_info = TokenInfo(Bool(False))
        return Map(
            {
                token_restricted: restricted_token_info,
                token_unrestricted: unrestricted_token_info,
            }
        )

    def init_storage(self):
        init_storage = super().init_storage()
        registrar = Address(self.default_registrar)

        init_storage.accounts = self.get_init_accounts(registrar)
        init_storage.security_tokens = self.get_init_security_tokens()

        country_restrictions = {
            Hex(country1): CountryRestriction(
                Nat(1), Timestamp("(time : timestamp)"), Nat(1), empty_map()
            ),
            Hex(country2): CountryRestriction(
                Nat(2), Timestamp("(time : timestamp)"), Nat(2), empty_map()
            ),
            Hex(country3): CountryRestriction(
                Nat(2), Timestamp("(time : timestamp)"), Nat(20), empty_map()
            ),
        }
        # Thanks to bigmaps, we can add a bunch of country restrictions with
        # essentially no impact on gas costs
        country_restrictions.update({
            Hex(i): CountryRestriction(
                Nat(2), Timestamp("(time : timestamp)"), Nat(20), empty_map()
            )
            for i in range(100000, 100200)
        })
        init_storage.country_restrictions = Map(country_restrictions)
        init_storage.kyc_registrars = Map({}, "int", "int")

        return init_storage


class Env(IssuerEnv):
    def __init__(
            self,
            ligo,
            tezos,
            owner = None,
            params: EnvParams = None,
        ):
        super().__init__(ligo, tezos, params, IssuerContract=Issuer)

    def setup_accounts(self, specs):
        for s in specs:
            params = Issuer.make_set_account_param(s)
            self.tezos.wait(self.issuer.ci.setAccount(params))

    def setup_env(self, invs, countries=None):
        """Sets up a testing environment with an accredited investor.
        for each investor, it registers it with the KYC registrar (self.kyc),
        itself registered as unrestricted with the issuer.

        Parameters
        ----------
        invs: array of tezos addresses
        countries: array of ints of the same length as invs. defaults to 1 for
            all investors if not provided"""
        if not countries:
            countries = [country1 for i in invs]

        for inv, country in zip(invs, countries):
            kyc_specs = [(inv, country, 12345, 10, 1, False)]
            self.parent_env.setup_env(kyc_specs)

            self.tezos.wait(
                self.issuer.ci.setRegistrar([self.kyc.addr, False])
            )

            account_specs = [(inv, self.kyc.addr, False)]
            self.setup_accounts(account_specs)

        return self



default_registrar = lambda tezos: tezos.addresses[1]


def test_is_owner(ligo, tezos):
    """Tests that restricted entrypoints can only be called from contract owner."""
    err_msg = "21"

    set_account_params = {
        "address_0": tezos.addresses[1],
        "registrar": tezos.addresses[2],
        "restricted": False,
    }
    entrypoints = [
        ["addToken", tezos.addresses[1]],
        ["setRegistrar", [tezos.addresses[1], False]],
        ["setToken", [tezos.addresses[1], False]],
        ["setAccount", set_account_params],
    ]

    e = Env(ligo, tezos)
    _test_is_owner(ligo, tezos, entrypoints, err_msg, ci=e.issuer.ci)


def test_add_token(ligo, tezos):
    """Tests that a token contract can be added."""
    e = Env(ligo, tezos)

    token = tezos.addresses[4]
    security_tokens = e.issuer.ci.storage()["security_tokens"].copy()
    security_tokens[token] = False
    tezos.wait(e.issuer.ci.addToken(token))
    assert e.issuer.ci.storage()["security_tokens"] == security_tokens


def test_set_token(ligo, tezos):
    """Tests that a token can be set to restricted or not."""
    e = Env(ligo, tezos)

    token = tezos.addresses[4]
    security_tokens = e.issuer.ci.storage()["security_tokens"].copy()
    security_tokens[token] = False
    tezos.wait(e.issuer.ci.addToken(token))

    tezos.wait(e.issuer.ci.setToken([token, True]))
    assert e.issuer.ci.storage()["security_tokens"][token] == True
    tezos.wait(e.issuer.ci.setToken([token, False]))
    assert e.issuer.ci.storage()["security_tokens"][token] == False


def test_set_registrars(ligo, tezos):
    """Adds a registrar."""
    e = Env(ligo, tezos)

    registrar = default_registrar(tezos)
    tezos.wait(e.issuer.ci.setRegistrar([registrar, False]))
    assert e.issuer.ci.storage()["kyc_registrars"] == {registrar: False}
    tezos.wait(e.issuer.ci.setRegistrar([registrar, True]))
    assert e.issuer.ci.storage()["kyc_registrars"] == {registrar: True}


def test_set_account(ligo, tezos):
    """Sets or adds an account."""
    e = Env(ligo, tezos)

    account = tezos.addresses[4]
    registrar = default_registrar(tezos)
    params = {
        "address_0": account,
        "registrar": registrar,
        "restricted": True,
    }

    tezos.wait(e.issuer.ci.setAccount(params))
    get_answer = lambda: {k: v for k, v in params.items() if k != "address_0"}
    assert e.issuer.ci.big_map_get("accounts/" + account) == get_answer()
    params["restricted"] = False
    tezos.wait(e.issuer.ci.setAccount(params))
    assert e.issuer.ci.big_map_get("accounts/" + account) == get_answer()

def test_update_country_restrictions(ligo, tezos):
    e = Env(ligo, tezos)

    country = country1
    params = Issuer.get_update_country_restrictions_params(
        country,
        10,
        2,
        {2: 10, 3: 5},
        '2021-01-01T00:00:00Z',
    )
    tezos.wait(e.issuer.ci.updateCountryRestrictions([params]))
    answer = {k: v for k, v in params.items() if k != "bytes_0"}
    assert e.issuer.ci.big_map_get('country_restrictions/' + country) == answer


def check_permissions(e, cb, inv):
    """Checks that a transaction callback fails by setting one of the investors
    to various fail cases."""
    # Test with country that is not in s.restricted_countries (should fail)
    inv_country_cache = e.kyc.ci.big_map_get("members/" + inv)["country"]
    e.tezos.wait(e.kyc.ci.setMemberCountry([inv, country3]))
    _test_error_msg(cb, "13")
    e.tezos.wait(e.kyc.ci.setMemberCountry([inv, inv_country_cache]))

    # KYC restricted accounts
    def set_kyc_restriction(inv, restriction, country=country1, min_rating=1):
        kyc_specs = [(inv, country, 12345, min_rating, 1, restriction)]
        add_kyc_member_params = KYC.make_add_members_param(kyc_specs)
        e.tezos.wait(e.kyc.ci.addMembers(add_kyc_member_params))

    # KYC country min rating
    set_kyc_restriction(inv, False, country2, 1)
    _test_error_msg(cb, "13")
    set_kyc_restriction(inv, False, country2, 10)

    # KYC restricted flag
    set_kyc_restriction(inv, True)
    _test_error_msg(cb, "14")
    set_kyc_restriction(inv, False)

    def setup_accounts(e, specs):
        for s in specs:
            params = Issuer.make_set_account_param(s)
            e.tezos.wait(e.issuer.ci.setAccount(params))

    # Issuer restricted account
    def set_issuer_restriction(inv, restriction):
        account_specs = [(inv, e.kyc.addr, restriction)]
        setup_accounts(e, account_specs)

    set_issuer_restriction(inv, True)
    _test_error_msg(cb, "23")
    set_issuer_restriction(inv, False)

    # Test with restricted registrars
    # Create unrestricted registrar
    e.tezos.wait(e.issuer.ci.setRegistrar([e.kyc.addr, True]))
    _test_error_msg(
        cb,
        "22",
    )
    e.tezos.wait(e.issuer.ci.setRegistrar([e.kyc.addr, False]))

    # Test that the token is not restricted
    # Set sender to unrestricted token
    _index = 1
    token = e.tezos.addresses[_index]
    e.tezos.wait(e.issuer.ci.addToken(token))
    e.tezos.wait(e.issuer.ci.setToken([token, True]))

    token_client = e.tezos.clients[_index]
    e.issuer.ci.checkTransfer.key = token_client.key
    assert (
        e.issuer.ci.checkTransfer.key.public_key_hash()
        in e.issuer.ci.storage()["security_tokens"].keys()
    )
    assert e.issuer.ci.storage()["security_tokens"][token]

    # TODO : Fake sender
    # _test_error_msg(cb, "This investor is not registered with an unrestricted KYC registrar known to the issuer.")
    e.tezos.wait(e.issuer.ci.setToken([token, False]))


def test_check_transfer(ligo, tezos):
    """Test that the issuer can burn tokens."""
    inv_from = tezos.addresses[0]
    inv_to = tezos.addresses[1]
    e = Env(ligo, tezos).setup_env([inv_from, inv_to])

    # Test that accredited investor can transfer to accredited investor
    def check_transfer(amount=22):
        tezos.wait(e.issuer.ci.checkTransfer([inv_from, inv_to,]))

    check_transfer()

    # Checking investor permissions for transferring to
    check_permissions(e, check_transfer, inv_from)
