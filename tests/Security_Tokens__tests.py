from pytest_tezos.TestTypes import Address, Map, Nat

from pynyx.environments import TokenEnv, EnvParams
from pynyx.contracts import Token as TokenContract, TokenParams

from Issuing_Entity__tests import check_permissions, Env as IssuerEnv
from helpers import _test_is_owner, _test_error_msg, assert_is_not_in_big_map


class Token(TokenContract):
    def init_storage(self):
        init_storage = super().init_storage()
        init_storage.balances = Map({Address(self.tezos.addresses[0]): Nat(100)})
        return init_storage


class Env(TokenEnv):
    def __init__(
            self,
            ligo,
            tezos,
            params: EnvParams = None,
        ):
        super().__init__(
            ligo,
            tezos,
            params,
            TokenContract=Token,
            ParentEnv=IssuerEnv,
        )


def test_is_owner(ligo, tezos):
    """Tests that restricted entrypoints can only be called from contract owner."""
    err_msg = "31"

    entrypoints = [
        ["mint", {'amount': 12, 'tr_to': tezos.addresses[0]}],
        ["burn", {'amount': 12, 'tr_to': tezos.addresses[0]}],
        ["setAllowTransferFrom", {
            'authority': tezos.addresses[0],
            'transfer_from_info': {'transfer_for': tezos.addresses[0], 'amount': 1},
            'remove': False
            }
         ],
    ]

    e = Env(ligo, tezos)
    _test_is_owner(ligo, tezos, entrypoints[:2], err_msg, ci=e.token.ci)

    del(entrypoints[0])
    del(entrypoints[1])
    _test_is_owner(ligo, tezos, entrypoints[2:], 'You are not allowed to allow transfer from for this investor', ci=e.token.ci)

Approval = lambda f, t, e, n: {
    "ap_from" : f,
    "ap_to" : t,
    "expires" : e,
    "tokens" : n,
}

def test_standalone(ligo, tezos):
    owner = tezos.addresses[0]
    token_params = TokenParams(owner=owner)
    token_params.standalone = True
    env_params = EnvParams(Token=token_params)
    e = TokenEnv(ligo, tezos, env_params)
    inv_from, inv_to, amount = tezos.addresses[0], tezos.addresses[2], 10

    assert_is_not_in_big_map(e.token.ci, 'balances', inv_from)
    assert_is_not_in_big_map(e.token.ci, 'balances', inv_to)
    tezos.wait(e.token.ci.mint({"tr_to": inv_from, "amount": amount}))
    assert e.token.ci.big_map_get(f'balances/{inv_from}') == amount
    tezos.wait(e.token.ci.transfer({"tr_to": inv_to, "amount": amount}))
    assert e.token.ci.big_map_get(f'balances/{inv_from}') == 0
    assert e.token.ci.big_map_get(f'balances/{inv_to}') == amount

def test_mint(ligo, tezos):
    """Test that the issuer can mint tokens."""
    inv = tezos.addresses[0]
    e = Env(ligo, tezos).setup_env([inv])

    # Test that owner can mint to accredited investor
    def mint(amount = 12):
        balance_before = e.token.ci.big_map_get('balances/'+inv)
        tezos.wait(e.token.ci.mint({'amount': amount, 'tr_to': inv}))
        assert e.token.ci.big_map_get('balances/' + inv) == balance_before + amount

    # Minting should work
    mint()

    # Trying to mint more than is available
    cb = lambda: mint(e.token.ci.storage()['tokens'] + 1)
    _test_error_msg(cb, "34")

    # Checking investor permissions for minting to
    check_permissions(e, mint, inv)

def test_burn(ligo, tezos):
    """Test that the issuer can burn tokens."""
    inv = tezos.addresses[0]
    e = Env(ligo, tezos).setup_env([inv])

    # Test that owner can burn to accredited investor
    def burn(amount = 12):
        balance_before = e.token.ci.big_map_get('balances/'+inv)
        tezos.wait(e.token.ci.burn({'amount': amount, 'tr_to': inv}))
        assert e.token.ci.big_map_get('balances/'+inv) == balance_before - amount

    # Burning should work
    burn()

    # Trying to burn more than is available
    cb = lambda: burn(e.token.ci.big_map_get('balances/'+inv) + 1)
    _test_error_msg(cb, "33")

    # Checking investor permissions for burning to
    check_permissions(e, burn, inv)


def allow_transfer_from(e, authority, transfer_for, amount, remove=False):
    transfer_from_info = {
        'transfer_for': transfer_for,
        'amount': amount,
    }
    set_allow_transfer_from_params = {
        'authority': authority,
        'transfer_from_info': transfer_from_info,
        'remove': remove
    }
    e.tezos.wait(e.token.ci.setAllowTransferFrom(set_allow_transfer_from_params))
    return transfer_from_info

def test_set_allow_transfer_from(ligo, tezos):
    """Tests that the owner can add an authority allowed to call transfer_from."""
    authority_index = 4
    authority = tezos.addresses[authority_index]
    e = Env(ligo, tezos)

    def set_allow_transfer_from(authority, transfer_for, amount, remove):
        transfer_from_info = allow_transfer_from(e, authority, transfer_for, amount, remove)
        if remove:
            assert_is_not_in_big_map(e.token.ci, 'allow_transfer_from', authority)
        else:
            assert transfer_from_info == e.token.ci.big_map_get('allow_transfer_from/'+authority)

    set_allow_transfer_from(authority, tezos.addresses[1], 1000, False)
    set_allow_transfer_from(authority, tezos.addresses[1], 1000, True)

def test_transfer(ligo, tezos):
    """Test that the issuer can transfer tokens."""
    sender_index = 0

    inv_from = tezos.addresses[sender_index]
    inv_to = tezos.addresses[1]
    e = Env(ligo, tezos).setup_env([inv_from, inv_to])


    token_client = e.tezos.clients[sender_index]
    e.issuer.ci.checkTransfer.key = token_client.key

    # Test that accredited investor can transfer to accredited investor
    def transfer(amount = 22):
        balance_from_before = e.token.ci.big_map_get('balances/'+inv_from)

        try:
            balance_to_before = e.token.ci.big_map_get('balances/'+inv_to)
        except:
            balance_to_before = 0

        tezos.wait(e.token.ci.transfer({
            'tr_to': inv_to,
            'amount': amount,
        }))
        assert e.token.ci.big_map_get('balances/'+inv_from) == balance_from_before - amount
        assert e.token.ci.big_map_get('balances/'+inv_to) == balance_to_before + amount

    # Transferring should work
    transfer()

    # Trying to transfer more than is available
    cb = lambda: transfer(e.token.ci.big_map_get('balances/'+inv_from) + 1)
    _test_error_msg(cb, "32")

    # Checking investor permissions for transferring to
    check_permissions(e, transfer, inv_from)

def test_transfer_from(ligo, tezos):
    """Test that the issuer can transfer tokens."""
    authority_index = 1
    tr_from_index = 0
    tr_to_index = 2

    authority = tezos.addresses[authority_index]
    inv_from = tezos.addresses[tr_from_index]
    inv_to = tezos.addresses[tr_to_index]
    e = Env(ligo, tezos).setup_env([inv_from, inv_to])

    authority_client = e.tezos.clients[authority_index]
    e.token.ci.transferFrom.key = authority_client.key

    # Test that accredited investor can transfer to accredited investor
    def transfer_from(amount = 22, inv_from = inv_from):
        balance_from_before = e.token.ci.big_map_get('balances/'+inv_from)
        try:
            balance_to_before = e.token.ci.big_map_get('balances/'+inv_to)
        except:
            balance_to_before = 0

        tezos.wait(e.token.ci.transferFrom({
            'tr_from': inv_from,
            'tr_to': inv_to,
            'amount': amount,
        }))

        assert e.token.ci.big_map_get('balances/'+inv_from) == balance_from_before - amount
        assert e.token.ci.big_map_get('balances/'+inv_to) == balance_to_before + amount

    ## Happy path

    amount = 20
    allow_transfer_from(e, authority, inv_from, amount, False)
    transfer_from(amount)
    assert e.token.ci.big_map_get('allow_transfer_from/'+authority)['amount'] == 0

    amount = 20
    allow_transfer_from(e, authority, inv_from, amount, False)
    answer = int(amount / 2)
    transfer_from(answer)
    assert e.token.ci.big_map_get('allow_transfer_from/'+authority)['amount'] == answer

    ## Fail cases

    cb = lambda: transfer_from(e.token.ci.big_map_get('balances/'+inv_from) + 1)
    _test_error_msg(cb, "37")

    cb = lambda: transfer_from(amount, inv_to)
    _test_error_msg(cb, "35" )

    # Remove the authorization for authority
    allow_transfer_from(e, authority, inv_from, amount, True)
    cb = lambda: transfer_from(amount, inv_from)
    _test_error_msg(cb, "36")

    # Checking investor permissions for transferring to
    allow_transfer_from(e, authority, inv_from, 100000, False)
    check_permissions(e, transfer_from, inv_from)
