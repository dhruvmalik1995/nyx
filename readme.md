**Nyx is the primordial goddess of the personified night and mother of
Destiny and Hope. She is the first divinity issued from the primordial
chaos.**

I take its references in the order that comes from chaos, the Chiasm
moover that took the ladder. Every innovation is misunderstood, as
easily as we misunderstand someone. NyX is the begining of something
new, something that will change our behaviors. We can't foresee it yet,
neither we foresaw looking at a live videos on our phones 20 years ago.
But its our Hope, the mission to do better, to cultivate our
curiousness, to always bring more efficiency and auditability, control
and privacy.

Hopefully, this endavour will bring more accessibility and liquidity in
financial markets at the benefit of the investors. They will allows them
to have, for the first time, a tool to follow their participation in
stocks, funds, bonds, art pieces… to track them and control them.

Let us proceed, onwards.

# Introduction

The Nyx contracts allow to handle on chain the transfer of assets
between investors. Nyx is composed of three contracts working together
to provide regulation compliant asset transfers by leveraging KYC
registrars and issuing entities on top of a ledger contract called
Security Token. This document details the motivation between each of
these contracts, along with instructions on how to deploy the contracts
and interact with them. We use `pytezos` to interact with Tezos, either
as a sandbox for testing, or on the test/main nets.

# Getting started

## Docker

The simplest way to get started is to clone the project, build the
docker image, and start docker compose. You'll then be able to `docker
exec` into the container with Ligo and all the python dependencies
installed, and run Nyx against the sandbox in the other container:

``` bash
$ git clone git@gitlab.com:equisafe/nyx.git
$ cd nxy/
$ docker build -t nyx .
$ docker-compose up
```

Docker compose created two containers:

``` bash
$ docker ps
CONTAINER ID        IMAGE
8be94b1a3cfb        nyx:latest
4915f1ce915a        yourlabs/tezos
```

To run the tests, `docker exec` into the nyx container:

``` bash
# Hop into the nyx container
$ docker exec -it 8be94b1a3cfb /bin/bash

# Run the registrar tests
root@:/nyx# py.test tests/KYC_Registrar__tests.py 
================================================== test session starts ==================================================
platform linux -- Python 3.8.2, pytest-5.4.1, py-1.8.1, pluggy-0.13.1
rootdir: /nyx
plugins: tezos-0.1.3, ligo-0.1.3
collected 4 items                                                                                                       

tests/KYC_Registrar__tests.py ....                                                                                [100%]

================================================== 4 passed in 20.00s ===================================================

# You can see the sandbox blocks as follows:
root@c018087cb8f7:/nyx# curl http://tz:8732/chains/main/blocks/
[["BMRpzYfeXfQgGP24s5LQxGovLbVhYkL961d1LUWj4T3aoCfeuLR"]]
```

From there, you can modify the Nyx source code from your host machine
and run it against the sandbox by jumping into a container shell.

We can also access the sandbox container:

``` bash
$ docker exec -it 4915f1ce915a /bin/bash

# You can now use tezos-client
bash-5.0$ tezos-client rpc get /chains/main/blocks/head
...
{ "protocol": "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb",
  "chain_id": "NetXjD3HPJJjmcd",
  "hash": "BL5nVaAa6i7Jj6yaTV8z9CCFYfRWVC9xaBX4xzeptWREnXD2ks1",
...
```

## Standalone dev

### Tezos sandbox

This section details how to setup the sandbox allowing to test the
contracts locally. We shall be using Yourlabs' docker image that
provides effortless access to the sandbox using port mapping between the
container and the host system:

``` bash
docker run -t -p 8732:8732 yourlabs/tezos  # add `-b` to run the container in "background" mode
```

We can easily verify that we can interact with the sandbox as follows:

``` bash
curl http://localhost:8732/chains/main/blocks/head/context/constants | jq .hard_gas_limit_per_operation
```

``` example
1040000
```

### Python

This documentation assumes that you have Python 3 available in your path
and that you have installed the project dependencies. For example:

``` python
pip install --user -r requirements.txt
```

# Contracts

We now detail the contracts one by one. We first introduce the
motivation behind each of these contracts before implementing examples.
The full API will be detailed in the following section (Interfaces).
Since the Security Token contract depends on the Issuing Entity
contract, which depends on the KYC Registrar contract, we introduce them
in the opposite order.

The main use behind the contracts is that they provide a mechanism to
enforce checks on investors exchanging assets. The Security Token
contract keeps track of which investors have which assets and provides
methods to transfer assets between each other. However, when
transferring assets, the Security Token asks the Issuing Entity to check
the eligibility of investors to transact between each other. One of
these checks consists in ensuring that both investors pass their KYC
check, meaning that the Issuing Entity contract needs to communicate
with the KYC Registrar contract. Other checks include the investor per
country count, the investor ratings, the restriction of KYC Registrars
or Security Tokens at the issuer level.

Now that we have a brief overview of how the contracts interact with
each other to allow or disallow asset transfers between investors, let
us introduce some concrete examples.

## KYC Registrar

The KYC Registrar contract keeps track of which investors are "KYC
restricted" or not. This is a stand-alone contract, that is, it does not
require any other contracts to be used and rather, provides a way for
other contracts to verify the KYC restrictions for an investor.

The contract storage consists in an `owner`, which is the only address
allowed to mutate the storage, and a `members` entry, which is a mapping
from investors (addresses) to investor information. The investor
information allows to set the `country`, `region`, `rating`, `exiration`
and (KYC) `restriction` for a given investor:

``` ocaml
type investor_info_t = {
    country    : country_t ;
    region     : region_t ;
    rating     : rating_t ;
    expires    : epoch_time_t ;
    restricted : restricted_t ;
    authority  : authority_id_t ;
}

type registrar_storage_t = {
    owner   : id_t ;
    members : ( id_t , investor_info_t ) map ;
}
```

![](emacs/images/kyc.png)

The simplest way to deploy this contract is to use the abstraction
implemented in the contract tests. We start by making some imports:

``` python
# Import Ligo and Tezos helper classes
from pytest_ligo import Ligo
from pytest_tezos import Tezos
tezos = Tezos()
```

We can now deploy the KYC contract with `environments.KYCEnv`:

``` python
# Import the KYC contract abstraction
from pynyx.contracts import ContractParams

owner = tezos.addresses[0]
kyc_params = ContractParams(owner)
```

``` python
from pynyx.environments import KYCEnv, EnvParams
env_params = EnvParams(KYC=kyc_params)
nyx = KYCEnv(Ligo(), tezos, env_params)
```

We can inspect the contract storage as follows:

``` python
nyx.kyc.ci.storage()
```

``` example
{'members': 180, 'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}
```

Note that the members storage entry is shown as an *integer*
representing the *big map ID* of the key. We'll see shortly how to
access elements from big maps.

Or by pretty printing it, which will show to be useful when dealing with
larger storages:

``` python
nyx.log_storage()
```

``` example
=== KYC storage ===

{'members': 180, 'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}


<pynyx.environments.KYCEnv object at 0x7f04d3e2e5b0>
```

And add and update members using the `addMembers` entrypoint:

``` python
import time
from pynyx.helpers import get_id_for_region_code

COUNTRIES = {
   "FR": get_id_for_region_code("FR-PAC"),
   "EN": get_id_for_region_code("GB-ENG"),
}

RATINGS = {
   "INDIVIDUAL": 1,
   "INSTITUTIONAL": 2,
}

current_timestamp = int(time.time())

members = [
    {
        "address_0": tezos.addresses[0],
        "country": COUNTRIES['FR'],
        "expires": 1245,
        "rating": 10,
        "region": 1,
        "restricted": False,
    },
    {
        "address_0": tezos.addresses[1],
        "country": COUNTRIES['FR'],
        "expires": 1245,
        "rating": 1,
        "region": 1,
        "restricted": False,
    },
    {
        "address_0": tezos.addresses[2],
        "country": COUNTRIES['EN'],
        "expires": 12345,
        "rating": 1,
        "region": 2,
        "restricted": True,
    },
]

tezos.wait(nyx.kyc.ci.addMembers(members))
nyx.log_storage()
```

``` example
=== KYC storage ===

{'members': 180, 'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}
```

We can now fetch the members information for `tezos.addresses[0]` with
PyTezos's `big_map_get` as follows:

``` python
nyx.kyc.ci.big_map_get(f"members/{tezos.addresses[0]}")
```

``` example
{'country': '504143', 'expires': '1970-01-01T00:20:45Z', 'rating': 10, 'region': 1, 'restricted': False}
```

## Issuing Entity

![](emacs/images/issuer.png)

The Issuing Entity contract keeps track of investor right with respect
to asset transfers. When transferring a token for instance (see next
section), the issuing entity is queried to check that the transfer is
allowed. In particular, it is the Issuing Entity contract's job to query
the various KYC Registrars associated with it for investor restrictions.

We can deploy an Issuing Entity with a KYC Registrar as follows:

``` python
from pynyx.environments import IssuerEnv
from pynyx.contracts import IssuerParams

issuer_params = IssuerParams(owner=owner, name="IssuerContract")
env_params = EnvParams(KYC=kyc_params, Issuer=issuer_params)

tezos = Tezos()
nyx = IssuerEnv(Ligo(), tezos, env_params)
```

``` python
nyx.log_storage()
```

``` example
=== KYC storage ===

{'members': 181, 'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}


=== Issuer storage ===

{ 'accounts': 182,
  'country_counters': {},
  'country_restrictions': 183,
  'document_hashes': 184,
  'global_invest_limit': 0,
  'investor_counter': 0,
  'kyc_registrars': {},
  'name': 'IssuerContract',
  'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
  'security_tokens': {}}


<pynyx.environments.IssuerEnv object at 0x7f04d3ce9850>
```

As you can see, the issuer has three big map entries in its storage
record: `accounts`, `country_restrictions` and `document_hashes`. We can
query each of the with `nyx.issuer.ci.big_map_get` as was done in the
KYC contract.

## Security Token

![](emacs/images/token.png)

The Security Token contract is the contract that keeps track of which
investor which assets. It is also the contract that handles the
transferring of assets between investors by querying the Issuing Entity
contract associated with it for investor rights. The Issuing Entity will
itself query the various KYC Registrars associated with it for investor
restrictions.

Let us now deploy a Security Token contract with the associated
environment that will take care of setting the Issuing Entity and KYC
Registrar contracts for us. Don't be afraid to look at the
implementation of `environments.TokenEnv` to see how the environment is
setup.

``` python
from pynyx.environments import TokenEnv
from pynyx.contracts import TokenParams

token_params = TokenParams(
    owner=owner,
    tokens=1000,   # init tokens
    symbol="NYX",
    decimals=2,
)
env_params = EnvParams(
    KYC=kyc_params,
    Issuer=issuer_params,
    Token=token_params,
)

tezos = Tezos()
nyx = TokenEnv(Ligo(), tezos, env_params)
nyx.log_storage()
```

``` example
=== KYC storage ===

{'members': 185, 'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}


=== Issuer storage ===

{ 'accounts': 186,
  'country_counters': {},
  'country_restrictions': 187,
  'document_hashes': 188,
  'global_invest_limit': 0,
  'investor_counter': 0,
  'kyc_registrars': {},
  'name': 'IssuerContract',
  'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
  'security_tokens': {}}


=== Token storage ===

{ 'agreement_procedure': False,
  'allow_transfer_from': 189,
  'balances': 190,
  'decimals': 2,
  'issuer': 'KT1MBHQF4aCmDZyfdrCnYcfrV2JamTgVare7',
  'name': 'Token',
  'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
  'standalone': False,
  'symbol': 'NYX',
  'tokens': 1000}
```

As you can see, the token contract storage includes two big map:
`balances` and `allow_transfer_from`. Both of them can be queried with
`nyx.token.ci.big_map_get`.

We can now populate our KYC registar as previously done:

``` python
tezos.wait(nyx.kyc.ci.addMembers(members))

from pynyx.helpers import LOG
LOG(nyx.kyc.ci.storage())
```

``` example
{'members': 185, 'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}
```

The environment already hooked the KYC registrar to the issuing entity
for us, but if we'd want to do it manually, we'd go about it like that:

``` python
LOG(nyx.issuer.ci.storage()['kyc_registrars'], "Before")
tezos.wait(nyx.issuer.ci.setRegistrar([nyx.kyc.addr, False]))
LOG(nyx.issuer.ci.storage()['kyc_registrars'], "After")
```

``` example
=== Before ===

{}


=== After ===

{'KT1RTvQXNouJJfPNxARzAreFmscocdSXhZfq': False}
```

We could restrict the registrar as follows:

``` python
tezos.wait(nyx.issuer.ci.setRegistrar([nyx.kyc.addr, True]))
LOG(nyx.issuer.ci.storage()['kyc_registrars'], "Set the registrar as restricted")
tezos.wait(nyx.issuer.ci.setRegistrar([nyx.kyc.addr, False]))
LOG(nyx.issuer.ci.storage()['kyc_registrars'], "And unrestrict it")
```

``` example
=== Set the registrar as restricted ===

{'KT1RTvQXNouJJfPNxARzAreFmscocdSXhZfq': True}


=== And unrestrict it ===

{'KT1RTvQXNouJJfPNxARzAreFmscocdSXhZfq': False}
```

Let's now add two unrestricted members, so we can have them exchange
token between each other shortly:

``` python
for i in range(2):
  account_params = {
    "address_0": tezos.addresses[i],
    "registrar": nyx.kyc.addr,
    "restricted": False,
  }
  tezos.wait(nyx.issuer.ci.setAccount(account_params))
  LOG(nyx.issuer.ci.big_map_get(f"accounts/{tezos.addresses[i]}"), f"{tezos.addresses[i]} account info")
```

``` example
=== tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx account info ===

{'registrar': 'KT1RTvQXNouJJfPNxARzAreFmscocdSXhZfq', 'restricted': False}


=== tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN account info ===

{'registrar': 'KT1RTvQXNouJJfPNxARzAreFmscocdSXhZfq', 'restricted': False}
```

And configure the countries investors are allowed to invest from:

``` python
from pynyx.contracts import Issuer

params = Issuer.get_update_country_restrictions_params(
    COUNTRIES["FR"],
    10,
    1,
    {1: 10, 2: 5},
    '2021-01-01T00:00:00Z',
)
tezos.wait(nyx.issuer.ci.updateCountryRestrictions([params]))
LOG(nyx.issuer.ci.big_map_get(f'country_restrictions/{COUNTRIES["FR"]}'))
```

``` example
{ 'country_invest_limit': 10,
  'min_rating': 1,
  'rating_restrictions': {1: 10, 2: 5},
  'vesting': '2021-01-01T00:00:00Z'}
```

The environment class already associated a security token contract with
`nyx.issuer` (accessible from `nyx.token`). But we could have done it
manually by associating a security token contract to the issuer:

``` python
tezos.wait(nyx.issuer.ci.addToken(nyx.token.addr))
LOG(nyx.issuer.ci.storage()['security_tokens'], "Add an unrestricted security token to the issuer")
tezos.wait(nyx.issuer.ci.setToken([nyx.token.addr, True]))
LOG(nyx.issuer.ci.storage()['security_tokens'], "And restrict it")
tezos.wait(nyx.issuer.ci.setToken([nyx.token.addr, False]))
LOG(nyx.issuer.ci.storage()['security_tokens'], "Or not")
```

``` example
=== Add an unrestricted security token to the issuer ===

{'KT1TMwoWMxLuqmTRFeP33ksmy1GtE6ZVrPKH': False}


=== And restrict it ===

{'KT1TMwoWMxLuqmTRFeP33ksmy1GtE6ZVrPKH': True}


=== Or not ===

{'KT1TMwoWMxLuqmTRFeP33ksmy1GtE6ZVrPKH': False}
```

We can now start minting some tokens \!

``` python
#LOG(nyx.token.ci.storage()['balances'], "Balances")
LOG(nyx.token.ci.storage()['tokens'], "Tokens left to mint")
amount, inv = 10, tezos.addresses[0]
tezos.wait(nyx.token.ci.mint({'amount': amount, 'tr_to': inv}))
LOG(nyx.token.ci.storage()['tokens'], f"Tokens after minting {amount} to {inv}")
LOG(nyx.token.ci.big_map_get(f'balances/{inv}'), f"Balances after minting {amount} to {inv}")
```

``` example
=== Tokens left to mint ===

1000


=== Tokens after minting 10 to tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx ===

990


=== Balances after minting 10 to tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx ===

10
```

We can now start transferring tokens between unrestricted investors
within the country restrictions and investor limits using
`nyx.token.ci.transfer`:

``` python
# We call `nyx.issuer.ci.transfer` from the address we just minted tokens to
investor_client = tezos.clients[0]
nyx.token.ci.transfer.key = investor_client.key

# And transfer tokens from this investor to some other investor
tr_to = tezos.addresses[1]
tezos.wait(nyx.token.ci.transfer({
    'tr_to': tr_to,
    'amount': 5,
}))
LOG(nyx.token.ci.big_map_get(f'balances/{inv}'), f"Balances after transfering tokens to {inv}")
LOG(nyx.token.ci.big_map_get(f'balances/{tr_to}'), f"Balances after transfering tokens to {tr_to}")
```

``` example
=== Balances after transfering tokens to tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx ===

5


=== Balances after transfering tokens to tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN ===

5
```

The security token contract owner can allow other addresses control some
amount of tokens on behalf an investor. We'll show in the next section
how to use `nyx.token.ci.transferFrom` from a crowdsale contract. Here's
a short snippet demonstrating how to use `~transferFrom`:

``` python
authority_index = 4
authority = tezos.addresses[authority_index]

transfer_from_info = {
    'transfer_for': inv,
    'amount': 5,
}
set_allow_transfer_from_params = {
    'authority': authority,
    'transfer_from_info': transfer_from_info,
    'remove': False
}
tezos.wait(nyx.token.ci.setAllowTransferFrom(set_allow_transfer_from_params))
LOG(nyx.token.ci.big_map_get(f'allow_transfer_from/{authority}'), f"Amount of tokens {authority} can transfer on behalf of {inv}")
```

``` example
=== Amount of tokens tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv can transfer on behalf of tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx ===

{'amount': 5, 'transfer_for': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}
```

We will now call `transfer_from` with the authority's key to send tokens
from `inv` to `tr_to`:

``` python
authority_client = tezos.clients[authority_index]
nyx.token.ci.transferFrom.key = authority_client.key

LOG(nyx.token.ci.big_map_get(f'allow_transfer_from/{authority}'), "Allowances before")
LOG(nyx.token.ci.big_map_get(f'balances/{tr_to}'), f"{authority} balance before")
LOG(nyx.token.ci.big_map_get(f'balances/{inv}'), f"{inv} balance before")
tezos.wait(nyx.token.ci.transferFrom({
    'tr_from': inv,
    'tr_to': tr_to,
    'amount': 3,
}))
LOG(nyx.token.ci.big_map_get(f'allow_transfer_from/{authority}'), "Allowances after")
LOG(nyx.token.ci.big_map_get(f'balances/{tr_to}'), f"{authority} balance after")
LOG(nyx.token.ci.big_map_get(f'balances/{inv}'), f"{inv} balance after")
```

``` example
=== Allowances before ===

{'amount': 5, 'transfer_for': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}


=== tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv balance before ===

5


=== tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx balance before ===

5


=== Allowances after ===

{'amount': 2, 'transfer_for': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}


=== tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv balance after ===

8


=== tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx balance after ===

2
```

### Stand-Alone Token

![](emacs/images/standalone-token.png)

We can of course use the Security Token as a Stand-Alone Token by
passing it the `TokenParams.standalone` parameter to `True`:

``` python
tezos = Tezos()
ligo = Ligo()
owner = tezos.addresses[0]
token_params = TokenParams(owner=owner, standalone=True)
env_params = EnvParams(Token=token_params)
nyx = TokenEnv(ligo, tezos, env_params)
nyx.log_storage()
```

``` example
=== Token storage ===

{ 'agreement_procedure': False,
  'allow_transfer_from': 191,
  'balances': 192,
  'decimals': 10,
  'issuer': 'tz1burnburnburnburnburnburnburjAYjjX',
  'name': 'Token',
  'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
  'standalone': True,
  'symbol': 'TK',
  'tokens': 100}
```

As we can see, the KYC and Issuing Entity contracts have not been
deployed. We can therefore start minting tokens to addresses skipping
the transfer checking procedures with the issuer and KYC registrar:

``` python
inv_from, inv_to, amount = tezos.addresses[0], tezos.addresses[2], 10

tezos.wait(nyx.token.ci.mint({"tr_to": inv_from, "amount": amount}))
assert nyx.token.ci.big_map_get(f'balances/{inv_from}') == amount
```

Let's now start transfering some tokens:

``` python
tezos.wait(nyx.token.ci.transfer({"tr_to": inv_to, "amount": amount}))
assert nyx.token.ci.big_map_get(f'balances/{inv_from}') == 0
assert nyx.token.ci.big_map_get(f'balances/{inv_to}') == amount
nyx.log_storage()
```

``` example
=== Token storage ===

{ 'agreement_procedure': False,
  'allow_transfer_from': 191,
  'balances': 192,
  'decimals': 10,
  'issuer': 'tz1burnburnburnburnburnburnburjAYjjX',
  'name': 'Token',
  'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
  'standalone': True,
  'symbol': 'TK',
  'tokens': 90}
```

## Delivery vs Payement

The `DvP.mligo` contract can be configured to allow for delivery versus
payement tasks ranging from crowdsales to decentralized exchanges. We'll
show how to use the contract to set those up.

### Crowdsale

When setup as a crowdsale, the delivery versus payement smart contract
can be hooked up to security tokens in stand-alone mode (no KYC nor
Issuer contracts) or not. We investigate both cases in the following
subsections.

1.  With a Stand-Alone Token
    
    ![](emacs/images/crowdsale-standalone-token.png)
    
    We now want the Crowdsale contract to trade some amount of Tz sent
    by an investor to the contract in exchange for an amount of security
    tokens equivalent in fiat (calculated from the crowdsale rates
    defined by `nyx.crowdsale.ci.storage()['tz_fiat_rate']` and
    `nyx.crowdsale.ci.storage()['token_fiat_rate']`). To do so, we need
    to give the authorization for the Crowdsale contract to trade from
    the organizer's balance in the Security Token contract. We need to
    set this up using the Crowdsale contract address and
    `nyx.token.ci.transferFrom`:
    
    ``` python
    from pynyx.environments import CrowdsaleEnv
    
    
    class Nyx(CrowdsaleEnv):
        def setup_env(self, invs = []):
            self.organizer = invs[0]
            self.invs = invs
    
            self._setup_tokens()
    
            return self
    
        def _setup_tokens(self):
            # Mint some tokens to the organizer
            amount = self.crowdsale.ci.storage()['tokens_max']
            self.tezos.wait(self.token.ci.mint({
                'amount': amount,
                'tr_to': self.organizer,
            }))
            # Allow the crowdsale contract to transfer the security token
            # funds on behalf of the organizer
            crowdsale = self.crowdsale.addr
            amount = self.crowdsale.ci.storage()['tokens_max']
            self.allow_transfer_from(crowdsale, self.organizer, amount)
    
        def allow_transfer_from(self, authority, transfer_for, amount, remove=False):
            transfer_from_info = {
                'transfer_for': transfer_for,
                'amount': amount,
            }
            set_allow_transfer_from_params = {
                'authority': authority,
                'transfer_from_info': transfer_from_info,
                'remove': remove
            }
            self.tezos.wait(self.token.ci.setAllowTransferFrom(set_allow_transfer_from_params))
            return transfer_from_info
    
    organizer_index = 0
    organizer = tezos.addresses[organizer_index]
    investor_index = 1
    investor = tezos.addresses[investor_index]
    env_params = EnvParams(
        Token=TokenParams(owner=organizer, standalone=True),
        Crowdsale=CrowdsaleParams(owner=organizer),
    )
    nyx = Nyx(Ligo(), Tezos(), env_params)
    nyx.setup_env([organizer, investor])
    nyx.log_storage()
    ```
    
    ``` example
    === Token storage ===
    
    { 'agreement_procedure': False,
      'allow_transfer_from': { 'KT1XUFKmLn7Wa1mP3zmWntkhg3paNQPtANhU': { 'amount': 100,
                                                                         'transfer_for': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}},
      'balances': {'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx': 100},
      'decimals': 10,
      'issuer': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
      'name': 'Token',
      'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
      'standalone': True,
      'symbol': 'TK',
      'tokens': 0}
    
    
    === Crowdsale storage ===
    
    { 'bonus_pct': [],
      'bonus_times': [],
      'crowdsale_completed': '1970-01-01T00:00:00Z',
      'crowdsale_finish': '2021-01-01T00:00:00Z',
      'crowdsale_start': '2020-01-01T00:00:00Z',
      'fiat': 0,
      'fiat_max': 100,
      'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
      'receiver': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
      'security_token': 'KT1Wy4mzUsBy3fKYSvwtGYTRKKK4eM2eWfqk',
      'token_fiat_rate': 3,
      'tokens': 0,
      'tokens_max': 100,
      'tz_fiat_rate': 2}
    ```
    
    We can now have the investor buy some tokens from the organizer
    through the Crowdsale contract:
    
    ``` python
    from decimal import Decimal
    
    get_tz_balances = lambda: {"organizer": tezos.clients[organizer_index].balance(), "investor": tezos.clients[investor_index].balance()}
    tz_balances_before = get_tz_balances()
    
    LOG(nyx.token.ci.storage()['balances'], "Security token balances BEFORE")
    
    pay_with = 5  # tz
    nyx.crowdsale.switch_key(tezos.clients[investor_index])
    resp = tezos.wait(nyx.crowdsale.ci.payable(None).with_amount(Decimal(pay_with)))
    
    tz_balances_after = get_tz_balances()
    
    LOG(nyx.token.ci.storage()['balances'], "Security token balances AFTER")
    LOG({k: tz_balances_after[k] - tz_balances_before[k] for k in tz_balances_before.keys()}, "Tz balance difference")
    ```
    
    ``` example
    === Security token balances BEFORE ===
    
    {'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx': 100}
    
    
    === Security token balances AFTER ===
    
    { 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx': 97,
      'tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN': 3}
    
    
    === Tz balance difference ===
    
    {'investor': Decimal('-517.080860'), 'organizer': Decimal('-507.000000')}
    ```

2.  With a Security Token
    
    ![](emacs/images/crowdsale.png)
    
    Let's now look at the crowdsale contract that hooks into the
    security token contract providing a security token holder with the
    ability to raise funds by selling tokens for Tz.
    
    To demonstrate how one can create a custom environment, we'll start
    over from scratch and create a `Nyx` class that will inherit from
    `pynyx.environments.CrowdsaleEnv` and override the `setup_env`
    method to setup the KYC members, the issuer accounts, the token
    holders, and the `transferFrom` accesses between the contract and
    the security token holder.
    
    We'll want to call `setup_env` with a crowdsale organizer and an
    investor, who will buy tokens sold by the crowdsale contract in
    exchange for Tz. The environment will need to deploy, on top of the
    Crowdsale contract, a Security Token contract, mint some tokens to
    the organizer, and allow the Crowdsale contract to transfer from the
    organizer's security token account to the crowdsale investor. We'll
    do this in method `Nyx._setup_tokens` called from `Nyx.setup_env`.
    We'll also create `Nyx._setup_kyc` to create a KYC account for the
    organizer and the investor, along with `Nyx._setup_issuer` which
    will setup the country restrictions and the communication between
    the Issuer and KYC contracts.
    
    ``` python
    class Nyx(Nyx):
        def setup_env(self, invs = []):
            self.organizer = invs[0]
            self.invs = invs
    
            self._setup_kyc()
            self._setup_issuer()
            self._setup_tokens()
    
            return self
    ```
    
    Let's now implement the setup helpers starting with the addition of
    the organizer and investor to the KYC contract:
    
    ``` python
    class Nyx(Nyx):
        def _setup_kyc(self):
            for inv in self.invs:
                rating, expires, region, restricted = 10, '2021-01-01T00:00:00Z', 1, False
                kyc_specs = [(inv, COUNTRIES["FR"], expires, rating, region, restricted)]
                self.parent_env.setup_env(kyc_specs)
    ```
    
    We now need to add the KYC contract to the Issuer and unrestrict it,
    add accounts and link them to the KYC contract, and allow accounts
    from France with rating greater that 0 to trade tokens.
    
    ``` python
    class Nyx(Nyx):
        def _setup_issuer(self):
            # Hook registrar
            self.tezos.wait(self.issuer.ci.setRegistrar({"0": self.kyc.addr, "1": False}))
    
            # Add accounts linked to a registrar
            for inv in self.invs:
                self.tezos.wait(self.issuer.ci.setAccount({"0": inv, "1": {"registrar": self.kyc.addr, "restricted": False}}))
    
            # Add permissive country restrictions
            country = COUNTRIES["FR"]
            params = [{
                "0": country,
                "1": {
                    "country_invest_limit": 10,
                    "vesting": '2021-01-01T00:00:00Z',
                    "min_rating": 1,
                    "rating_restrictions": {}
                }
            }]
            self.tezos.wait(self.issuer.ci.updateCountryRestrictions(params))
    ```
    
    Finally, we can define a crowdsale organizer and an investor,
    instantiate Nyx, and call its `setup_env` method:
    
    ``` python
    organizer_index = 4
    investor_index = 3
    investor = tezos.addresses[investor_index]
    organizer = tezos.addresses[organizer_index]
    
    nyx = Nyx(Ligo(), Tezos()).setup_env([organizer, investor])
    ```
    
    Let's check on the storage and witness that it is correctly setup:
    
    ``` python
    nyx.log_storage()
    ```
    
    ``` example
    === KYC storage ===
    
    { 'members': { 'tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv': { 'country': '504143',
                                                             'expires': '2021-01-01T00:00:00Z',
                                                             'rating': 10,
                                                             'region': 1,
                                                             'restricted': False},
                   'tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv': { 'country': '504143',
                                                             'expires': '2021-01-01T00:00:00Z',
                                                             'rating': 10,
                                                             'region': 1,
                                                             'restricted': False}},
      'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}
    
    
    === Issuer storage ===
    
    { 'accounts': { 'tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv': { 'registrar': 'KT1FHkqDCvazFuGb6hCSQ3drfANbt9tT3Fgt',
                                                              'restricted': False},
                    'tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv': { 'registrar': 'KT1FHkqDCvazFuGb6hCSQ3drfANbt9tT3Fgt',
                                                              'restricted': False}},
      'country_counters': {},
      'country_restrictions': { '504143': { 'country_invest_limit': 10,
                                            'min_rating': 1,
                                            'rating_restrictions': {},
                                            'vesting': '2021-01-01T00:00:00Z'}},
      'document_hashes': {},
      'global_invest_limit': 0,
      'investor_counter': 0,
      'kyc_registrars': {'KT1FHkqDCvazFuGb6hCSQ3drfANbt9tT3Fgt': False},
      'name': 'Issuer',
      'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
      'security_tokens': {}}
    
    
    === Token storage ===
    
    { 'agreement_procedure': False,
      'allow_transfer_from': { 'KT1J5VpqnHUTPkXGf3k6HELyEpEgVX8jxhrJ': { 'amount': 100,
                                                                         'transfer_for': 'tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv'}},
      'balances': {'tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv': 100},
      'decimals': 10,
      'issuer': 'KT1ECdVG7VFpTdrHU49btQ76q7EVgxFwjaRn',
      'name': 'Token',
      'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
      'symbol': 'TK',
      'tokens': 0}
    
    
    === Crowdsale storage ===
    
    { 'bonus_pct': [],
      'bonus_times': [],
      'crowdsale_completed': '1970-01-01T00:00:00Z',
      'crowdsale_finish': '2021-01-01T00:00:00Z',
      'crowdsale_start': '2020-01-01T00:00:00Z',
      'fiat': 0,
      'fiat_max': 100,
      'owner': 'tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv',
      'receiver': 'tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv',
      'security_token': 'KT1Hox9L5FozatryXm25reGiJ8r3b3YeXhCT',
      'token_fiat_rate': 3,
      'tokens': 0,
      'tokens_max': 100,
      'tz_fiat_rate': 2}
    
    
    <__main__.Nyx object at 0x7fb300f00a90>
    ```
    
    We now want the investor to buy 5tz worth of security token from the
    crowdsale contract. Since the `token_fiat_rate = 3` and
    `tz_fiat_rate = 2`, the organizer should recieve 5tz in his account
    after the transaction, and the investor should have `int(5tz *
    (2fiat/tz) / (3token/fiat)) = 3 tokens` in the security token
    balances.
    
    ``` python
    from decimal import Decimal
    
    get_tz_balances = lambda: {"organizer": tezos.clients[organizer_index].balance(), "investor": tezos.clients[investor_index].balance()}
    tz_balances_before = get_tz_balances()
    
    LOG(nyx.token.ci.storage()['balances'], "Security token balances BEFORE")
    
    pay_with = 5  # tz
    nyx.crowdsale.switch_key(tezos.clients[investor_index])
    resp = tezos.wait(nyx.crowdsale.ci.payable(None).with_amount(Decimal(pay_with)))
    
    tz_balances_after = get_tz_balances()
    
    LOG(nyx.token.ci.storage()['balances'], "Security token balances AFTER")
    LOG({k: tz_balances_after[k] - tz_balances_before[k] for k in tz_balances_before.keys()}, "Tz balance difference")
    ```
    
    ``` example
    === Security token balances BEFORE ===
    
    { 'tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv': 0,
      'tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv': 100}
    
    
    === Security token balances AFTER ===
    
    { 'tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv': 3,
      'tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv': 97}
    
    
    === Tz balance difference ===
    
    {'investor': Decimal('-5.000000'), 'organizer': Decimal('5.000000')}
    ```
    
    As expected, 3 security tokens were transferred from the crowdsale
    organizer to the investor in exchange for 5tz.

### Exchange

![](emacs/images/exchange.png)

The DvP contract can also be configured to allow exchanging between
multiple tokens. What we'll be doing is create.

The exchange works as follows:

1.  There is an exchange contract, E, with an owner, O. O has accounts
    with some funds in various tokens contracts and allows the exchange
    contract to transfer some amount on his behalf.
2.  An investor, I, wants to exchange n tokens A into tokens B. He will:
    a. Allow E to transfer n tokens from contract A on his behalf. b.
    Call the `payable` entrypoint on E asking to exchange n tokens A
    into m tokens (where m is calculated with the suitable rates defined
    in E's storage). c. E will then use the transfer from allowance to
    transfer n tokens in A to O, the exchange owner. d. E will calculate
    m in with the relevant rates e. E will send m tokens in B, from O's
    account to I's.

We'll start by importing the exchange environment and parameter data
structure:

``` python
from pynyx.environments import ExchangeEnv
from pynyx.contracts import ExchangeParams
```

We'll now create our own environment inheriting from `ExchangeEnv` which
will setup accounts for the exchange in the various tokens supported by
the exchange.

`ExchangeEnv` provides a `setup_env` helper method that sets up the
different rates we want. We'll be inheriting from this class and
overload `setup_env` to also mint some tokens to the exchange owner, and
setup transfer from allowances so that the exchange contract can
transfer tokens on it's owner accounts:

``` python
from typing import List

class Nyx(ExchangeEnv):
    def setup_env(self, fiat_rates: List[int], tokens_maxs: List[int], token_owner_index: int):
        amount, receiver = 50, self.exchange.params.receiver

        # Mint tokens to organizer
        for i, token_env in enumerate(self.token_envs):
            token_env.token.switch_key(self.tezos.clients[token_owner_index])
            self.tezos.wait(token_env.token.ci.mint({'amount': amount, 'tr_to': receiver}))

        # Setup transfer from
        for token_env in self.token_envs:
            self.allow_transfer_from(token_env, self.exchange.addr, receiver, amount)

        # Add tokens to exchange
        super().setup_env(fiat_rates, tokens_maxs, 3, 100)

        return self


    def allow_transfer_from(self, e, authority, transfer_for, amount, remove=False):
        transfer_from_info = {
            'transfer_for': transfer_for,
            'amount': amount,
        }
        set_allow_transfer_from_params = {
            'authority': authority,
            'transfer_from_info': transfer_from_info,
            'remove': remove
        }
        e.tezos.wait(e.token.ci.setAllowTransferFrom(set_allow_transfer_from_params))
```

As we shall see, the exchange environment works slightly differently
than the previous ones in that it has multiple token environment
associated with it.

We'll setup three stand-alone tokens so that we do not have to setup the
KYC and Issuer accounts in this sections. Let's define an exchange
organizer (the contract owner), an investor, and an owner for all three
token contracts.

``` python
indices = [1, 0, 3]
organizer_index, investor_index, token_owner_index = indices
organizer, investor, token_owner = [tezos.addresses[i] for i in indices]
```

We can now instantiate a list of token environments:

``` python
token_env_params = EnvParams(
    Token=TokenParams(owner=token_owner, standalone=True, tokens=1000),
)
token_envs = [TokenEnv(ligo, tezos, token_env_params) for i in range(3)]
```

Finally, we deploy our exchange and call `setup_env`:

``` python
exchange_env_params = EnvParams(
    Exchange=ExchangeParams(owner=organizer, receiver=organizer),
)
nyx = Nyx(ligo, tezos, exchange_env_params, token_envs=token_envs)
nyx.setup_env([2, 3, 4], [100, 200, 300], token_owner_index)
```

To make it simpler to track which tokens we are manipulating, let's
assign two tokens to variables:

``` python
token_envA, token_envB = [nyx.token_envs[i] for i in range(2)]
```

We'll now mint some token A to the investor by switching to the token
owner key before calling the method:

``` python
# Mint some tokens to investor
amount = 10
token_envA.token.switch_key(tezos.clients[token_owner_index])
tezos.wait(token_envA.token.ci.mint({'amount': amount, 'tr_to': investor}))
```

Since big maps only allow us to request a value from its key, we cannot
have a dictionary of all entries without constructing on. Which is what
we are doing bellow

``` python
# Save storages for balance assertions after exchanging between currencies
def balances_safe_get(ci, account):
    try:
        return ci.big_map_get('balances/' + account)
    except:
        return 0

token_balances_before = {
    token_name: {
        account: balances_safe_get(token_env.token.ci, account)
        for account in [investor, organizer]
    }
    for token_name, token_env in [('A', token_envA), ('B', token_envB)]
}

LOG(token_balances_before, "Token balances before")
```

``` example
=== Token balances before ===

{ 'A': { 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx': 10,
         'tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN': 160},
  'B': { 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx': 11,
         'tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN': 139}}
```

We now have the investor allow the exchange contract to transfer
`amount` tokens A on its behalf and then call `payable` on the exchange
asking to convert `amount` tokens A into tokens B:

``` python
# Allow contract to transfer `amount` form investor to itself
token_envA.token.switch_key(tezos.clients[investor_index])
nyx.allow_transfer_from(token_envA, nyx.exchange.addr, investor, amount)
# Exchange `amount`
nyx.exchange.switch_key(tezos.clients[investor_index])
tezos.wait(nyx.exchange.ci.payable(token_envA.token.addr, token_envB.token.addr, amount))
```

Bellow are some assertions showing that everything occurred smoothly:

``` python
fiat_rateA = nyx.exchange.ci.storage()['currencies'][token_envA.token.addr]['fiat_rate']
fiat_rateB = nyx.exchange.ci.storage()['currencies'][token_envB.token.addr]['fiat_rate']
delta_currB = int(amount * fiat_rateA / fiat_rateB)
assert token_envA.token.ci.big_map_get('balances/'+investor) == token_balances_before['A'][investor] - amount
if investor in token_balances_before['A'].keys():
    assert token_envB.token.ci.big_map_get('balances/'+investor) == token_balances_before['B'][investor] + delta_currB
else:
    assert token_envB.token.ci.big_map_get('balances/'+investor) == delta_currB
assert token_envA.token.ci.big_map_get('balances/'+organizer) == token_balances_before['A'][organizer] + amount
assert token_envB.token.ci.big_map_get('balances/'+organizer) == token_balances_before['B'][organizer] - delta_currB

organizer_tz_before = tezos.clients[organizer_index].balance()
investor_token_before = balances_safe_get(token_envB.token.ci, investor)
organizer_token_before = token_envB.token.ci.big_map_get('balances/'+organizer)
```

We can of course also convert between Tz and tokens. We proceed the same
way as before, but use the `ZERO_ADDRESS` to indicate that we want to
transfer from or to Tz. In this example, we want to exchange 5 tz to
tokens B.

``` python
from pynyx.helpers import ZERO_ADDRESS
from decimal import Decimal
pay_with, _ = 5, 0

tezos.wait(nyx.exchange.ci.payable(ZERO_ADDRESS, token_envB.token.addr, _).with_amount(Decimal(pay_with)))
```

We can now assert that the exchange organizer has received the proper
amount of Tz, 5:

``` python
organizer_tz_after = tezos.clients[organizer_index].balance()
assert organizer_tz_after - organizer_tz_before == Decimal(pay_with)
```

Finally, we assert that the investor gets credited with the correct
amount of tokens B:

``` python
investor_token_after = token_envB.token.ci.big_map_get('balances/'+investor)
organizer_token_after = token_envB.token.ci.big_map_get('balances/'+organizer)

fiat_rateTZ = nyx.exchange.ci.storage()['currencies'][ZERO_ADDRESS]['fiat_rate']
delta_currB = int(pay_with * fiat_rateTZ / fiat_rateB)

assert investor_token_after - investor_token_before == delta_currB
assert organizer_token_after - organizer_token_before == -delta_currB
```

## Going further

If you're interested in looking into making your own environments, or
tayloring the Nyx contracts to your own needs, be sure to dive into the
code. Bellow is a dependency graph of the repo's python code that shows
how the code is organized:

![](emacs/images/tests_deps.png)

# Interfaces

## Fundamental types

``` ocaml
type limit_t = nat
type rating_t = nat
type country_t = nat
type region_t = nat
type counter_t = nat
type id_t = bytes
type amount_t = nat
type restricted_t = bool
type agreement_procedure_t = bool
type name_t = string
type registrar_t = address
type token_t = address
type owner_t = address
type issuer_t = address
type investor_t = address
type epoch_time_t = timestamp
type custodian_t = address
type reg_key_t = nat
type set_t = bool
type investor_id_t = bytes
type authority_id_t = bytes
type hash_t = bytes
```

### Helpers

``` ocaml
type require_t = (cond : bool) * (error_msg : string) -> unit
```

## Modular

Functionalities can be added to the token and issuing entity contracts.
The mechanism is described in this section as a sole contract. Note that
in terms of implementation, the modular functionality is not a
stand-alone contract, but rather additions to storages and entrypoints
of contracts that wish to become modular.

### Storage

``` ocaml
type module_t = address
type entrypoint_t = string
type permission_t = bool
type active_t = bool

type hook_info_t = {
  tag_bools : bool list ;
  permitted : permission_t ;
  active    : active_t ;
  always    : bool ;
}

type hooks_t = (entrypoint_t , hook_info_t) map
type permissions_t = (entrypoint_t , permission_t) map

type module_info_t = {
  active      : active_t ;
  set         : bool ;
  hooks       : hooks_t;
  permissions : permissions_t;
}

type active_modules_t = module_t set
type module_data_t = (module_t , module_info_t) map

type modules_info_t = {
  active_modules : active_modules_t ;
  module_data    : module_data_t ;
}
```

### Entrypoints

``` ocaml
type attach_module_arg_t = module_t
type detach_module_arg_t = module_t
```

## Multi sig

### Storage

``` ocaml
type authority_t = {
  signatures          : (bytes , bool) map ;
  multi_sig_auth      : (bytes , address list) map;
  multi_sig_threshold : limit_t ;
  address_count       : counter_t ;
  approved_until      : epoch_time_t ;
  restricted          : restricted_t;
}

type multisig_storage_t = {
  id_map         : (investor_t , address_info_t) map ;
  authority_data : (authority_id_t , authority_t) map ;
  owner_id       : investor_id_t ;
}
```

## Registrar

### Address info

``` ocaml
type address_info_t = {
  id         : id_t ;
  restricted : restricted_t ;
}
```

### Investor ID & Investor info

``` ocaml
type investor_id_t = bytes
type investor_info_t = {
    country    : country_t ;
    region     : region_t ;
    rating     : rating_t ;
    expires    : epoch_time_t ;
    restricted : restricted_t ;
    authority  : authority_id_t ;
}
```

### Authority info

``` ocaml
type authority_info_t = {
  multiSigAuth      : (bytes , address list) map;
  countries         : country_t list ;
  multiSigThreshold : nat ;
  addressCount      : nat ;
  restricted        : bool ;
}
```

### Storage

``` ocaml
type registrar_storage_t = {
  owner          : id_t ;
  members        : ( id_t , investor_info_t ) map ;
  id_map         : ( address , address_info_t ) map
  investor_data  : ( investor_id_t , investor_info_t ) map ;
  authority_data : ( authority_id_t , authority_info_t ) map ;
}
```

### Entrypoints

1.  Storage Mutation
    
    We provide storage mutations that are defined as:
    
    ``` ocaml
    type function_name_arg_t = arg_type
    (* Leads to entrypoint `functionName` *)
    let function_name (p: function_name_arg_t) (s: registrar_storage_t) : function_name_ret_t = ...
    ```
    
    In cases where `function_name_ret_t` is not explicitely defined in
    the document, as per the above example, it is assumed to be defined
    as `type function_name_ret_t = (operation list *
    registrar_storage_t)` for mutations defined in this section.
    
    We define bellow the public storage mutation for the contract by
    their argument following the above convention. As an example, the
    `add_members_t` argument presented in the snippet bellow correspond
    to the contract entrypoint `addMembers`. This entrypoint should be
    called with an argument of type `add_members_t` that can be obtained
    by defining in Ligo and compiling to Michelson.
    
    ``` ocaml
    (* The addMembers entrypoint also works for updating *)
    type add_member_arg_t = ( investor_t * investor_info_t ) list
    type remove_members_arg_t = investor_t list
    type check_member_arg_t = investor_t
    type register_addresses_arg_t = investor_t * (address list)
    type restrict_addresses_arg_t = investor_t * (address list)
    ```

### Private functions

1.  Getters
    
    ``` ocaml
    get_id_t : investor_t -> id_t
    get_investor_t : investor_t -> investor_info_t
    get_rating_t : id_t -> rating_t
    get_region_t : id_t -> region_t
    get_country_t : id_t -> country_t
    get_expires_t : id_t -> expires_t
    ```

2.  Others
    
    ``` ocaml
    is_premitted_t : address -> bool
    is_premitted_id_t : id_t -> bool
    authority_check_t : country_t * registrar_storage_t -> unit
    generate_id_t : string -> bytes
    add_addresses_t : ms_id_t * addresses_t -> id_map_t * nat
    check_sender_is_owner_t : address_t -> unit
    ```

## Issuing entity

### Storage

``` ocaml
type country_info_t = {
  counts       : nat list ;
  limits       : limit_t list ;
  permitted    : bool ;
  min_rating   : rating_t ;
}

type account_info_t = {
  count      : counter_t ;
  rating     : rating_t ;
  reg_key    : reg_key_t ;
  set        : set_t ;
  restricted : restricted_t ;
  custodian  : custodian_t ;
}

type token_info_t = {
  set        : set_t ;
  restricted : restricted_t ;
}

type document_info_t = {
  hash      : hash_t ;
  timestamp : epoch_time_t ;
}

type registrar_info_t = {
  kyc_registrar : registrar_t ;
  restricted    : restricted_t ;
}

type issuing_entity_storage_t = {
  name            : name_t ;
  owner           : owner_t ;

  locked          : bool ;
  kyc_registrars  : registrar_info_t set ;
  counts          : nat list ;
  limits          : limit_t list ;
  countries       : ( country_t , country_info_t ) map ;
  accounts        : (investor_id_t , account_info_t) map ;
  security_tokens : (token_t , token_info_t) map ;
  documents       : (string , document_info) map;
}
```

### Entrypoints

1.  Storage mutation
    
    ``` ocaml
    type update_global_limit_arg_t = limit_t
    type update_country_restrictions_arg_t = (country_t * country_restriction_t) list
    type set_entity_restriction_arg_t = account_id_t * restricted_t
    type set_token_restriction_arg_t = token_t * restricted_t
    type add_token_arg_t = token_arg_t
    type remove_token_arg_t = token_arg_t
    type set_registrars_arg_t = registrar_t * restricted_t
    type is_registered_investor_arg_t = investor_t
    type set_document_hash_arg_t = document_id_t * document_hash_t
    type remove_document_arg_t = document_id_t
    ```

### Private functions

1.  Getters
    
    ``` ocaml
    get_document_hash : document_id_t -> hash_t
    get_document_timestamp : document_id_t -> epoch_time_t
    get_all_documents : issuing_entity_storage_t -> hash_t list
    ```

2.  Others
    
    ``` ocaml
    fail_if_sender_not_owner : issuing_entity_storage_t) -> unit
    ```

## Security Token

### Approvals

``` ocaml
type approval_t = {
  ap_from  : investor_t | issuer_t ; (* approval from  *)
  ap_to    : investor_t ;
  expires  : epoch_time_t ;
  tokens   : amount_t ; (*amount allowed*)
  executed : executed_t ;
}
```

### Storage

``` ocaml
type security_token_storage_t = {
  module_info         : module_info_t ;

  name                : name_t ;
  symbol              : string ;
  decimals            : nat ;
  owner_id            : id_t ;
  issuer              : issuer_t ;
  agreement_procedure : agreement_procedure_t ;
  balances            : ( investor_t , amount_t ) map ;
  total_supply        : amount_t ;
  authorized_supply   : amount_t ;

  cust_balances       : ( address , ( address , nat ) map ) map ;
  allowed             : ( address , ( address , nat ) map ) map ;

  modules_info        : modules_info_t ;
}
```

### Entrypoints

1.  Storage mutation
    
    ``` ocaml
    type add_approval_arg_t = approval_t
    type transfer_arg_t = {
      tr_from : investor_t ;
      tr_to   : investor_t ;
      amount  : amount_t ;
    }
    type burn_arg_t = {
      tr_to  : investor_t ;
      amount : amount_t ;
    }
    type mint_arg_t = {
      tr_to  : investor_t ;
      amount : amount_t ;
    }
    
    type set_authorized_supply_arg_t = amount_t
    
    type check_transfer_arg_t = {
      auth_id   : id_t ;
      id        : id_t ;
      custodian : custodian_t ;
      addresses : address list ;
      rating    : rating list ;
      country   : country list ;
      value     : amount_t ;
    }
    
    type transfer_from_arg_t = {
      from  : address ;
      to    : address ;
      value : amount_t ;
    }
    
    type allowance_arg_t = {
      owner   : owner_t;
      spender : investor_t ;
    }
    
    type custodian_balance_of_arg_t = {
      owner     : owner_t ;
      custodian : custodian_t ;
    }
    
    type check_transfer_custodian_arg_t = {
      custodian : custodian_t ;
      from      : investor_t ;
      to        : investor_t ;
      value     : amount ;
    }
    
    type transfer_custodian_arg_t = {
      sender   : address ;
      receiver : address :
      value    : amount_t ;
    }
    ```

### Private functions

1.  Getters
    
    ``` ocaml
    balance_of : investor_t -> amount_t
    get_total_supply : security_token_storage_t -> amount_t
    get_authorized_supply : security_token_storage_t -> amount_t
    ```

2.  Private
    
    ``` ocaml
    check_approval_list : transfer_t -> approval_t list -> approval_t list option
    make_transfer : transfer_t -> investor_amount_t -> investor_amount_t
    check_permitted : security_token_storage_t -> unit
    ```

## Custodian

### Storage

This contract has an empty storage. Internally, it works by querying a
Security Token contract, and hence indirectly uses its storage.

### Entrypoints

``` ocaml
type balance_of_arg_t = (token_t * investor_t) * (nat -> operation list)
type check_custodian_transfer_arg_t = token_t * investor_t * investor_t * amount_t
type transfer_arg_t = token_t * investor_t * investor_t
type recieve_transfer_arg_t = investor_t * amount_t
type transfer_internal_arg_t = token_t * investor_t * investor_t * amount_t
```

## Security token modules

Modules available share a common core that we introduce before detailing
the modules APIs.

### Module core

1.  Storage
    
    ``` ocaml
    type st_module_core_storage_t = {
      owner_id : id_t ;
      owner : owner_t ;
      security_token : token_t ;
      issuer : issuer_t ;
    }
    ```

### Dividend

1.  Storage
    
    ``` ocaml
    type beneficiary = investor_t
    
    type dividend_storage_t = {
      name            : string ;
      dividend_amount : amount_t ;
      clainExpiration : epoch_time_t ;
      claimed         : ( address , bool ) map ;
      claimed         : ( address , ( address , bool ) map ) map ;
      module_info     : st_module_core_storage_t ;
    }
    ```

2.  Entrypoints
    
    ``` ocaml
    type issue_dividend_arg_t = epoch_time_t
    type claim_dividend_arg_t = beneficiary_t
    type claim_many_arg_t = beneficiary_t list
    type claim_custodian_dividend_arg_t = beneficiary_t * custodian_t
    type claim_many_custodian_arg_t = beneficiary_t list * custodian_t
    type close_dividend_arg_t = ()
    ```

### Vested Options

1.  Storage
    
    ``` ocaml
    type option_info_t = {
      amount         : amount_t ;
      exercise_price : amount_t ;
      creation_date  : epoch_time_t ;
      vest_date      : epoch_time_t ;
    }
    
    type peg_t = amount
    type option_id_t = id_t
    type dividend_storage_t = {
      total_options            : amount_t ;
      tz_peg                   : peg_t ;
      expiry_date              : epoch_time_t ;
      termination_grace_period : epoch_time_t ;
      receiver                 : investor_t ;
      option_data              : ( option_id_t , option_info_t list) ;
      options                  : ( option_id_t , amount_t ) map ;
      module_info              : st_module_core_storage_t ;
    }
    ```

2.  Entrypoints
    
    We first define some new types before introducing the storage:
    
    ``` ocaml
    type exercise_price_t = nat
    type vest_date_t = epoch_time_t
    ```
    
    ``` ocaml
    type modify_peg_arg_t = peg_t
    type issue_options_arg_t = id_t * amount_t * exercise_price_t * vest_date_t
    type accelerate_vesting_date_arg_t = investor_id_t * ( option_id_t list ) * vest_date_t
    type exercise_options_arg_t = option_id_t list
    type cancel_expired_options_arg_t = investor_id_t
    type terminate_options_arg = investor_id_t
    ```
