(**** storage types ****)


type registrar_info_t = {
  restricted : restricted_t ;
}
type registrars_t = (registrar_t , registrar_info_t) map
type token_info_t = {
  restricted : restricted_t ;
}
type tokens_t = (token_t , token_info_t) map
type rating_counter_t  = (rating_t  , counter_t) map
type country_counter_t = {
  global_counter : counter_t ;
  rating_counter : rating_counter_t ;
}
type country_counters_t = (country_t , country_counter_t) map
type rating_restriction_t  = (rating_t  , limit_t) map
type country_restriction_t = {
  country_invest_limit : limit_t ;
  min_rating   : rating_t ;
  rating_restrictions : rating_restriction_t ;
  vesting      : epoch_time_t ;
}
type country_restrictions_t = (country_t, country_restriction_t) big_map

type account_info_t = {
  registrar : registrar_t ;
  restricted : restricted_t ;
}
type account_t = address
type accounts_t = (account_t , account_info_t) big_map

type document_id_t = string
type document_hash_t = bytes
type document_hashes_t = (document_id_t , document_hash_t) big_map

type issuing_entity_storage_t = {
  name            : name_t ;
  owner           : owner_t ;
  kyc_registrars  : registrars_t ;
  security_tokens : tokens_t ;
  accounts        : accounts_t ;
  (*restrictions*)
  global_invest_limit : limit_t ;
  country_restrictions : country_restrictions_t ;
  (*counters*)
  investor_counter : counter_t ;
  country_counters  : country_counters_t ;
  document_hashes : document_hashes_t ;
}


(**** I/O types ****)

(** other **)

type update_global_limit_t = limit_t
type update_country_restrictions_t = (country_t * country_restriction_t) list
type add_token_t = token_t
type remove_token_t = (token_t * (investor_t * investor_info_t))
type set_registrars_t = registrar_t * restricted_t
type set_account_t = account_t * account_info_t
type set_token_t = token_t * restricted_t
type check_transfer_t = investor_t * investor_t
type issuing_entity_ep_t =
  | UpdateGlobalLimit of update_global_limit_t
  | UpdateCountryRestrictions of update_country_restrictions_t
  | AddToken of add_token_t
  | SetRegistrar of set_registrars_t
  | SetAccount of set_account_t
  | SetToken of set_token_t
  | CheckTransfer of check_transfer_t

type issuing_entity_ret_t = (operation list * issuing_entity_storage_t)
