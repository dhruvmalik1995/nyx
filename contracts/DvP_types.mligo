type fiat_rate_t = nat  (* fiat^-1 *)

type currency_t = address
type currency_info_t = {
  fiat_rate  : fiat_rate_t ;
  tokens_max : amount_t ;
  tokens     : amount_t ;
}
type currencies_t = ( currency_t , currency_info_t ) map

type dvp_storage_t = {
  owner               : address ;
  receiver            : address ;
  security_token      : address ;
  crowdsale_start     : epoch_time_t ;
  crowdsale_finish    : epoch_time_t ;
  crowdsale_completed : epoch_time_t ;
  bonus_pct           : nat list ;
  bonus_times         : nat list ;

  currencies          : currencies_t ;
  (*
  tz_fiat_rate        : fiat_rate_t ;
  token_fiat_rate     : fiat_rate_t ;
  tokens_max          : limit_t ;
  tokens              : amount_t ;
  *)
  fiat_max            : limit_t ;
  fiat                : amount_t ;
}

type manual_sale_t = address * amount_t * fiat_rate_t
type set_tz_fiat_rate_t = fiat_rate_t
type is_open_t = unit
type can_participate_t = address

type curr_to = currency_t
type curr_from = currency_t
type payable_t = curr_from * curr_to * amount_t
type set_currency_t = currency_t * fiat_rate_t * amount_t * amount_t

type dvp_ep_t =
  | IsOpen of is_open_t
  | CanParticipate of can_participate_t
  | Payable of payable_t
  | SetCurrency of set_currency_t

type dvp_ret_t = (operation list * dvp_storage_t)
