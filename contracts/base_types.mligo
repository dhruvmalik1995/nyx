(**** simple type aliases****)

type limit_t = nat
type rating_t = nat
type country_t = bytes
type region_t = nat
type counter_t = nat
type id_t = string
type amount_t = nat
type restricted_t = bool
// type executed_t = bool
type agreement_procedure_t = bool
type name_t = string
type registrar_t = address
type token_t = address
type owner_t = address
type issuer_t = address
type investor_t = address
type epoch_time_t = timestamp
type custodian_t = address
type set_t = bool
type investor_id_t = bytes
type authority_id_t = bytes
type authority_t = address
