type value_t = int
type owner_t = address
type storage_t = {
  value : value_t ;
  owner : owner_t ;
}

type return_t = (operation list) * storage_t


let increment (a: unit) (s: storage_t): return_t =
  let contract : unit contract = Operation.get_entrypoint "%increment" s.owner in
  let op : operation = Operation.transaction () 1mutez contract in
  ( ([op] : operation list), s)

let set_module_hooks (a: unit) (s: storage_t) : return_t =
  let contract : string set contract = Operation.get_entrypoint "%setModuleHooks" s.owner in
  let entrypoints = (Set.add "modularIncrement" (Set.empty: string set)) in
  let op : operation = Operation.transaction entrypoints 1mutez contract in
  ( ([op] : operation list), s)

type action =
  | Increment of unit
  | SetModuleHooks of unit

let main (p : action) (storage : storage_t): return_t =
  match p with
  | Increment n -> increment () storage
  | SetModuleHooks n -> set_module_hooks () storage
