#include "base_types.mligo"
#include "helpers.mligo"
#include "Security_Tokens_types.mligo"

let token_errors = {
    not_owner = "31" ;                     // "Only owner or issuer can call this method."
    balance_not_sufficient = "32" ;        // "Emitter balance in not sufficient."
    not_enough_tokens_to_burn = "33" ;     // "not enough tokens to burn"
    not_enough_tokens_to_mint = "34" ;     // "not enough tokens to mint"
    transfer_from_not_allowed = "35";      // "You are not allowed to allow transfer from for this investor"
    transfer_from_call_not_allowed = "36"; // "You are not allowed to call transfer from."
    transfer_from_amount_error = "37";     // "You cannot transfer this amount of tokens on behalf of this investor."
}

let check_transfer (tr_from, tr_to, issuer, standalone: address * address * address * standalone_t) : operation list =
  if standalone then
    ( [] : operation list )
  else
    let contract : (investor_t * investor_t) contract = Operation.get_entrypoint "%checkTransfer" issuer in
    [Operation.transaction (tr_from , tr_to) 0mutez contract]

let make_transfer (transfer, balances : transfer_from_t * investor_amount_t)
    : investor_amount_t =
  let recipient_bal =
    match Big_map.find_opt transfer.tr_to balances with
    | None -> 0n
    | Some n -> n
  in
  let emitter_bal   = Big_map.find transfer.tr_from balances in
  if (emitter_bal < transfer.amount) then
    let throw = failwith token_errors.balance_not_sufficient in
    balances
  else
    let new_emitter_bal   = abs (emitter_bal - transfer.amount) in
    let new_recipient_bal = recipient_bal + transfer.amount in
    let new_balances =
      Big_map.update transfer.tr_to (Some new_recipient_bal)
        (Big_map.update transfer.tr_from (Some new_emitter_bal) balances)
    in
    new_balances

let transfer_helper (p, s: transfer_from_t * security_token_storage_t)
  : security_token_ret_t =
  let check_transfer_op = check_transfer ( p.tr_from , p.tr_to , s.issuer , s.standalone ) in
  let new_balance = make_transfer (p , s.balances) in
  let new_s =
    {
      name = s.name ;
      allow_transfer_from = s.allow_transfer_from ;
      issuer = s.issuer ;
      tokens = s.tokens ;
      agreement_procedure = s.agreement_procedure ;
      balances = new_balance ;
      symbol = s.symbol ;
      decimals = s.decimals ;
      owner = s.owner ;
      standalone = s.standalone ;
    }
  in
  ( check_transfer_op , new_s )

let check_permitted (s: security_token_storage_t): unit =
  if (
    sender <> s.issuer
    && sender <> s.owner
  ) then failwith token_errors.not_owner else unit


(*
   Strangely, we have to redefine `check_permitted` because we get a gas exhaustion
   error when calling `transfer` if we do not. Indeed, we call this ownership check
   from `mint` and `burn`. We found out that if we call this check only in `mint`
   or `burn`, the `transfer` function works properly. If we call it from both, we
   get the gas error. My guess is that the Ligo compiler inlines the function if
   it is called from a single place, but does not if called from multiple places.
   The strange solution, is to redefine `check_permitted` as `check_permitted2`.
   Calling the former in `mint` and the later in `burn` saves us from the gas
   error. Note that the `transfer` function that fails with a gas error never calls
   either `burn` nor `mint`...
*)
let check_permitted2 (s: security_token_storage_t): unit =
  if (
    sender <> s.issuer
    && sender <> s.owner
  ) then failwith token_errors.not_owner else unit


let set_allow_transfer_from_ (p, s: set_allow_transfer_from_t * security_token_storage_t) =
  let new_allow_transfer_from =
    if p.remove then
      Big_map.remove p.authority s.allow_transfer_from
    else
      Big_map.update p.authority (Some p.transfer_from_info) s.allow_transfer_from
  in
  let new_s = {
    name = s.name ;
    allow_transfer_from = new_allow_transfer_from ;
    issuer = s.issuer ;
    tokens = s.tokens ;
    agreement_procedure = s.agreement_procedure ;
    balances = s.balances ;
    symbol = s.symbol ;
    decimals = s.decimals ;
    owner = s.owner ;
    standalone = s.standalone ;
  } in
  ( ([] : operation list) , new_s )

let set_allow_transfer_from (p, s: set_allow_transfer_from_t * security_token_storage_t) =
  let is_allowed =
    if (
      sender <> s.issuer
      && sender <> s.owner
      && sender <> p.transfer_from_info.transfer_for
    ) then failwith token_errors.transfer_from_not_allowed else unit
  in
  set_allow_transfer_from_ ( p , s )

let transfer (p, s : transfer_t * security_token_storage_t)
    : security_token_ret_t =
  transfer_helper ( { tr_from = Tezos.sender ; tr_to = p.tr_to ; amount = p.amount ; } , s )

let transfer_from (p, s : transfer_from_t * security_token_storage_t)
  : security_token_ret_t =
  (* check that Tezos.sender is allowed to transfer p.amount for p.tr_from *)
  let transfer_from_info =
    match Big_map.find_opt Tezos.sender s.allow_transfer_from with
    | Some p -> p
    | None -> (failwith token_errors.transfer_from_call_not_allowed : transfer_from_info_t)
  in
  let check1 = require (transfer_from_info.transfer_for = p.tr_from) token_errors.transfer_from_not_allowed in
  let check2 = require (transfer_from_info.amount >= p.amount) token_errors.transfer_from_amount_error in
  (* Subtract amount form transfer_from allowance *)
  let new_transfer_from_info = {
    transfer_for = transfer_from_info.transfer_for ;
    amount = abs (transfer_from_info.amount - p.amount) ;
  } in
  let empty_ops, tmp_s = set_allow_transfer_from_ ( { authority = Tezos.sender ; transfer_from_info = new_transfer_from_info ; remove = false ; } , s ) in
  transfer_helper ( { tr_from = p.tr_from ; tr_to = p.tr_to ; amount = p.amount ; } , tmp_s )

let burn (p, s: burn_t * security_token_storage_t)
    : security_token_ret_t =
  let check_sender = check_permitted2 s in
  let check_transfer_op = check_transfer ( p.tr_to , p.tr_to , s.issuer , s.standalone ) in
  let new_balance =
    let recipient_bal = Big_map.find p.tr_to s.balances in
    let burnt = (* abs (recipient_bal - p.amount) in *)
      if (p.amount > recipient_bal) then
        let throw = failwith token_errors.not_enough_tokens_to_burn in
        recipient_bal
      else
        abs (recipient_bal - p.amount) in
    Big_map.update p.tr_to (Some burnt) s.balances
  in
  let new_s = {
    name = s.name ;
    allow_transfer_from = s.allow_transfer_from ;
    issuer = s.issuer ;
    tokens = s.tokens ;
    agreement_procedure = s.agreement_procedure ;
    symbol = s.symbol ;
    decimals = s.decimals ;
    owner = s.owner ;
    balances = new_balance ;
    standalone = s.standalone ;
  } in
  ( (check_transfer_op : operation list) , new_s )

let mint (p, s: mint_t * security_token_storage_t)
    : security_token_ret_t =
  let check_sender = check_permitted s in
  let check_transfer_op = check_transfer ( p.tr_to , p.tr_to , s.issuer , s.standalone ) in
  let new_tokens =
    if (p.amount > s.tokens) then
      let throw = failwith token_errors.not_enough_tokens_to_mint in
      s.tokens
    else
      abs (s.tokens - p.amount) in
  let new_balance =
    match Big_map.find_opt p.tr_to s.balances with
    | None -> Big_map.update p.tr_to (Some (p.amount)) s.balances
    | Some n -> Big_map.update p.tr_to (Some (n + p.amount)) s.balances
  in
  let new_s = {
    name = s.name ;
    allow_transfer_from = s.allow_transfer_from ;
    issuer = s.issuer ;
    tokens = new_tokens ;
    agreement_procedure = s.agreement_procedure ;
    symbol = s.symbol ;
    decimals = s.decimals ;
    owner = s.owner ;
    balances = new_balance ;
    standalone = s.standalone ;
  } in
  ( (check_transfer_op : operation list) , new_s )

(* private method

The contract is designed with a storage that does not have a TotalSupply
attribute. But we can recalculate it as follows
*)
let get_total_supply (s: security_token_storage_t)
    : amount_t =
  0n
  //let aux = fun (prev, balance : amount_t * (investor_t * amount_t)) ->
  //  prev + balance.1 in
  //Map.fold aux s.balances 0n

(* private method

The contract is designed with a storage that does not have an AuthorizedSupply
attribute. But we can recalculate it as follows
*)
let get_authorized_supply (s: security_token_storage_t)
    : amount_t =
  s.tokens + (get_total_supply s)

(* private method *)
let balance_of (p: investor_t) (s: security_token_storage_t)
    : amount_t =
  Big_map.find p s.balances

let main (param, s : security_token_ep_t * security_token_storage_t)
    : security_token_ret_t =
  match param with
  | Transfer p -> transfer ( p , s )
  | Burn p -> burn (p , s)
  | Mint p -> mint (p , s)
  | SetAllowTransferFrom p -> set_allow_transfer_from ( p , s )
  | TransferFrom p -> transfer_from ( p , s )
