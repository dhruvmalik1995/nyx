type contract_t = address
type permission_t = bool

type permissions_t = contract_t set
type message_t = string
type message_info_t = {
  contract : contract_t ;
  message : message_t ;
}
type message_key_t = nat
type messages_t = ( message_key_t , message_info_t ) big_map

type eventsink_storage_t = {
  owner : owner_t ;
  permissions : permissions_t ;
  messages : messages_t ;
  message_counter : counter_t ;
}

type set_permission_arg_t = contract_t * permission_t
type post_event_arg_t = message_t
type flush_messages_arg_t = unit

  type eventsink_ep_t =
  | SetPermission of set_permission_arg_t
  | PostEvent of post_event_arg_t
  | FlushMessages of flush_messages_arg_t
