type ms_id_t = bytes
type address_info_t = {
  id : ms_id_t ;
  restricted : restricted_t ;
}

type authority_t = {
  signatures : (bytes , bool) map ;
  multi_sig_auth : (bytes , address list) map;
  multi_sig_threshold : limit_t ;
  address_count : counter_t ;
  approved_until : epoch_time_t ;
  restricted : restricted_t;
}

(*
	bytes32 public ownerID;
	mapping (address => Address) idMap;
	mapping (bytes32 => Authority) authorityData;
*)
