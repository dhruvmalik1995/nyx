#include "base_types.mligo"
#include "KYC_Registrar_types.mligo"
#include "Security_Tokens_types.mligo"
#include "Issuing_Entity_types.mligo"
#include "helpers.mligo"

let issuer_errors = {
    not_owner = "21" ;                          // "Only owner can call this entrypoint"
    investor_not_registered_with_unrestricted_registrar = "22" ;  // "This investor is not registered with an unrestricted KYC registrar known to the issuer."
    investor_restricted_at_issuer_lvl = "23" ;  // "Investor is restricted at issuer level"
    authority_err_msg = "24" ;                  // "You are not allowed to call this entrypoint."
}

let fail_if_sender_not_owner (s : issuing_entity_storage_t) : unit =
  let check_sender = if (sender <> s.owner) then failwith issuer_errors.not_owner else () in
  check_sender


let update_global_limit (p, s :update_global_limit_t * issuing_entity_storage_t)
  : issuing_entity_ret_t =
  let check_sender = fail_if_sender_not_owner s in
  let new_s =
    { name = s.name ;
      owner = s.owner ;
      kyc_registrars = s.kyc_registrars ;
      security_tokens = s.security_tokens ;
      global_invest_limit = p ;
      country_restrictions = s.country_restrictions ;
      investor_counter = s.investor_counter ;
      country_counters = s.country_counters ;
      accounts = s.accounts ;
      document_hashes = s.document_hashes ;
    } in
  ( ([] : operation list) , new_s )


let update_country_restrictions (p, s : update_country_restrictions_t * issuing_entity_storage_t)
  : issuing_entity_ret_t =
  let check_sender = fail_if_sender_not_owner s in
  let new_restrictions =
    let aux = fun (prev, el: country_restrictions_t * (country_t * country_restriction_t)) ->
      Big_map.update el.0 (Some el.1) prev
    in
    List.fold aux p s.country_restrictions
  in
  let new_s =
    { name = s.name ;
      owner = s.owner ;
      kyc_registrars = s.kyc_registrars ;
      security_tokens = s.security_tokens ;
      global_invest_limit = s.global_invest_limit ;
      country_restrictions = new_restrictions ;
      investor_counter = s.investor_counter ;
      country_counters = s.country_counters ;
      accounts = s.accounts ;
      document_hashes = s.document_hashes ;
    } in
  ( ([] : operation list) , new_s )

let add_token (p, s : add_token_t * issuing_entity_storage_t)
  : issuing_entity_ret_t =
  let check_sender = fail_if_sender_not_owner s in
  let addable_token = p in
  let default_token_info =
    {
      restricted = false ;
    } in
  let new_security_tokens = Map.update
      addable_token (Some default_token_info) s.security_tokens in
  let new_s = {
    name = s.name ;
    owner = s.owner ;
    kyc_registrars = s.kyc_registrars ;
    security_tokens = new_security_tokens ;
    global_invest_limit = s.global_invest_limit;
    country_restrictions = s.country_restrictions ;
    investor_counter = s.investor_counter ;
    country_counters =  s.country_counters ;
    accounts = s.accounts ;
    document_hashes = s.document_hashes ;
  } in
  ( ([] : operation list) , new_s )

let set_registrars (p, s : set_registrars_t * issuing_entity_storage_t)
  : issuing_entity_ret_t =
  let check_sender = fail_if_sender_not_owner s in
  let registrar = p.0 in
  let restricted = p.1 in
  let new_registrar_info =
    {
      restricted = restricted ;
    }
  in
  let new_kyc_registrars = Map.update registrar (Some new_registrar_info) s.kyc_registrars in
  let new_s = {
    name = s.name ;
    owner = s.owner ;
    kyc_registrars = new_kyc_registrars ;
    security_tokens = s.security_tokens ;
    global_invest_limit = s.global_invest_limit;
    country_restrictions = s.country_restrictions ;
    investor_counter = s.investor_counter ;
    country_counters =  s.country_counters ;
    accounts = s.accounts ;
    document_hashes = s.document_hashes ;
  } in
  ( ([] : operation list) , new_s )

let set_account (p, s: set_account_t * issuing_entity_storage_t)
  : issuing_entity_ret_t =
  let check_sender = fail_if_sender_not_owner s in
  let account, new_account_info = p in
  let new_accounts = Big_map.update account (Some new_account_info) s.accounts in
  let new_s =
    { name = s.name ;
      owner = s.owner ;
      kyc_registrars = s.kyc_registrars ;
      security_tokens = s.security_tokens ;
      global_invest_limit = s.global_invest_limit ;
      country_restrictions = s.country_restrictions ;
      investor_counter = s.investor_counter ;
      country_counters = s.country_counters ;
      accounts = new_accounts ;
      document_hashes = s.document_hashes ;
    } in
  ( ([] : operation list), new_s)

let set_token (p, s : set_token_t * issuing_entity_storage_t)
  : issuing_entity_ret_t =
  let check_sender = fail_if_sender_not_owner s in
  let token = p.0 in
  let new_restricted = p.1 in
  let token_info = Map.find token s.security_tokens in
  let new_token_info =
    {
      restricted = new_restricted ;
    } in
  let new_security_tokens = Map.update
      token (Some new_token_info) s.security_tokens in
  let new_s =
    { name = s.name ;
      owner = s.owner ;
      kyc_registrars = s.kyc_registrars ;
      security_tokens = new_security_tokens ;
      global_invest_limit = s.global_invest_limit ;
      country_restrictions = s.country_restrictions ;
      investor_counter = s.investor_counter ;
      country_counters = s.country_counters ;
      accounts = s.accounts ;
      document_hashes = s.document_hashes ;
    } in
  ( ([] : operation list), new_s)

(* Private *)
let check_investors (invs, s : investor_t set * issuing_entity_storage_t) : operation list =
  let n_unrestricted_registrars =
    let aux = fun (counter, r: nat * (registrar_t * registrar_info_t)) ->
      let registrar_info = r.1 in
      if registrar_info.restricted then
        counter
      else
        counter + 1n
    in
    Map.fold aux s.kyc_registrars 0n
  in
  let check_no_registrars =
    if n_unrestricted_registrars = 0n
    then failwith issuer_errors.investor_not_registered_with_unrestricted_registrar
  in
  let aux = fun (acc, inv: operation list * investor_t) ->
    let check_inv_restriction =
      match Big_map.find_opt inv s.accounts with
      | Some inv_info ->
        if inv_info.restricted
        then failwith issuer_errors.investor_restricted_at_issuer_lvl
        else ()
      | None -> ()
    in

    (* Check on all registrars *)
    let registrar_check_result =
      let aux = fun (acc, r: (operation list * counter_t) * (registrar_t * registrar_info_t)) ->
        let registrar_info = r.1 in
        let ops, counter = acc in
        if registrar_info.restricted then
          (ops , counter)
        else
          let registrar = r.0 in
          let contract : (investor_t * country_restrictions_t * bool) contract = Operation.get_entrypoint "%checkMember" registrar in
          let is_last = if counter + 1n = n_unrestricted_registrars then true else false in
          let op : operation = Operation.transaction (inv , s.country_restrictions, is_last) 0mutez contract in
          (op::ops , counter + 1n)
      in
      Map.fold aux s.kyc_registrars (([] : operation list) , 0n)
    in
    let ops = registrar_check_result.0 in
    let aux = fun (acc, op: operation list * operation) ->
      op::acc
    in
    List.fold aux registrar_check_result.0 acc

  in
  Set.fold aux invs ([] : operation list)

let check_token_restriction (token_info : token_info_t) : unit =
  if  token_info.restricted then
    failwith("FOOO")
  else
    ()

let check_transfer (p, s : check_transfer_t * issuing_entity_storage_t) =
  //let check_st_restr = check_token_restriction (Map.find sender s.security_tokens) in
  let debitor = p.0 in
  let creditor = p.1 in
  let investors = (Set.add debitor (Set.add creditor (Set.empty : investor_t set))) in
  let kyc_check = check_investors ( investors , s ) in
  ( kyc_check , s )

(* Private method *)
let get_document_hash (p, s : document_id_t * issuing_entity_storage_t)
  : document_hash_t = Big_map.find p s.document_hashes

let main (param, s : issuing_entity_ep_t * issuing_entity_storage_t)
  : issuing_entity_ret_t =
  match param with
  | AddToken p -> add_token (p , s)
  | SetRegistrar p -> set_registrars (p , s)
  | SetAccount p -> set_account (p , s)
  | SetToken p -> set_token (p , s)
  | CheckTransfer p -> check_transfer (p , s)
  (* To test *)
  | UpdateGlobalLimit p -> update_global_limit (p , s)
  | UpdateCountryRestrictions p -> update_country_restrictions (p , s)
